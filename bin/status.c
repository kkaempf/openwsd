/*
 * status.c
 * 
 * Get scanner status
 * 
 * Copyright (c) 2019 Klaus Kämpf <kkaempf@gmail.com>
 *
 */

#ifdef HAVE_CONFIG_H
#include <wsd_config.h>
#endif

#include <u/uuid.h>
#include <assert.h>
#include <wsd-debug.h>
#include <wsd-xml.h>
#include <wsd-xml-binding.h>
#include <wsd-names.h>
#include <wsd-client-api.h>

int
main(int argc, char *argv[])
{
  WsdRequest *request;
  WS_LASTERR_Code error;
  debug_add_handler(wsd_debug_message_handler, DEBUG_LEVEL_ALWAYS, NULL);

  WsdClient *client = wsd_client_create_from_url("http://192.168.0.112:80/ws/");
  request_opt_t *options = wsd_options_create();
  wsd_set_options_flags(options, FLAG_DUMP_REQUEST);
  request = wsd_action_get_scanner_description(client, options);
  if (!request) {
    fprintf(stderr, "Request creation failed\n");       
  }
  else {
    error = wsd_get_last_error(request);
    if (error != WS_LASTERR_OK) {
      char *error_str = wsd_transport_get_last_error_string(error);
      fprintf(stderr, "No reponse: error %d:%s\n", error, error_str);
    }
    WsXmlDocH doc = wsd_response_doc(request);
    WsXmlNodeH node = wsd_response_node(request);
    xml_parser_element_dump(stderr, doc, node);
  }
  wsd_request_destroy(request);
  wsd_client_destroy(client);
  wsd_options_destroy(options);
  return 0;
}
