/*******************************************************************************
 * Copyright (C) 2004-2006 Intel Corp. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
1;5B1;5B *  - Neither the name of Intel Corp. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL Intel Corp. OR THE CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

/**
 * @author Anas Nashif
 * @author Vadim Revyakin
 */

#ifdef HAVE_CONFIG_H
#include <wsd_config.h>
#endif


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include "u/libu.h"
#include "wsd-client-transport.h"
#include "wsd-soap.h"
#include "wsd-client.h"
#include "wsd-client-transport.h"

static char *auth_methods[] = {
  _WS_NO_AUTH,
  _WS_BASIC_AUTH,
  _WS_DIGEST_AUTH,
  _WS_PASS_AUTH,
  _WS_NTLM_AUTH,
  _WS_GSSNEGOTIATE_AUTH,
	NULL,
};


extern void wsd_handler(WsdRequest *rq, void *user_data);

#ifdef BENCHMARK
static long long transfer_time = 0;
#endif

#if 1
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <ctype.h>

void
hexdump (unsigned char *ptr, int size, FILE *out)
/*
 * hex dump 'size' bytes starting at 'ptr'
 */
{
    unsigned char *lptr = ptr;
    int count = 0;
    long start = 0;

    while (size-- > 0)
    {
	if ((count % 16) == 0)
	    fprintf (out, "\t%08lx:", start);
	fprintf (out, " %02x", *ptr++);
	count++;
	start++;
	if (size == 0)
	{
	    while ((count%16) != 0)
	    {
		fprintf(out, "   ");
		count++;
	    }
	}
	if ((count % 16) == 0)
	{
	    fprintf (out, " ");
	    while (lptr < ptr)
	    {
	        unsigned char c = ((*lptr&0x7f) < 32)?'.':(*lptr & 0x7f);
		fprintf (out, "%c", c);
		lptr++;
	    }
	    fprintf(out,"\n");
	}
    }
    if ((count % 16) != 0)
	fprintf(out, "\n");

    fflush (out);
    return;
}
#endif

int
wsd_send_request(WsdRequest *rq, u_buf_t **image_buf)
{
  int ret = 0;
#ifdef BENCHMARK
  struct timeval tv0, tv1;
  long long t0, t1;
#endif

#ifdef BENCHMARK
  gettimeofday(&tv0, NULL);
#endif

  wsd_handler(rq, NULL);
  if (rq->last_error != WS_LASTERR_OK) {
    rq->response_doc = NULL;
    warning("Couldn't send request to client: %s\n", rq->fault_string);
    ret = 1;
  }
  rq->response_doc = wsd_build_envelope_from_response(rq, image_buf);
#ifdef BENCHMARK
  gettimeofday(&tv1, NULL);
  t0 = tv0.tv_sec * 10000000 + tv0.tv_usec;
  t1 = tv1.tv_sec * 10000000 + tv1.tv_usec;
  transfer_time += t1 - t0;
#endif
  return ret;
}

#ifdef BENCHMARK
long long
get_transfer_time()
{
  long long l = transfer_time;
  transfer_time = 0;
  return l;
}
#endif


const char *
wsdc_transport_get_auth_name(wsdc_auth_type_t auth)
{
	switch (auth) {
	case WS_NO_AUTH:
		return _WS_NO_AUTH;
	case WS_BASIC_AUTH:
		return _WS_BASIC_AUTH;
	case WS_DIGEST_AUTH:
		return _WS_DIGEST_AUTH;
	case WS_PASS_AUTH:
		return _WS_PASS_AUTH;
	case WS_NTLM_AUTH:
		return _WS_NTLM_AUTH;
	case WS_GSSNEGOTIATE_AUTH:
		return _WS_GSSNEGOTIATE_AUTH;
	default:;
	}
	return "Unknown";
}

void
wsdc_transport_set_auth_request_func(WsdClient * cl,
				  wsdc_auth_request_func_t f)
{
	cl->authentication.auth_request_func = f;
}


void
wsdc_transport_set_agent(WsdClient * cl, const char *arg)
{
	u_free(cl->user_agent);
	cl->user_agent = arg ? u_strdup( arg ) : NULL;
}

char *
wsdc_transport_get_agent(WsdClient * cl)
{
	if (cl->user_agent)
		return u_strdup(cl->user_agent);
	else
		return u_strdup(DEFAULT_USER_AGENT);
}


void
wsdc_transport_set_proxy(WsdClient * cl, const char *arg)
{
	u_free(cl->proxy_data.proxy);
	cl->proxy_data.proxy = arg ? u_strdup( arg ) : NULL;
}

char *
wsdc_transport_get_proxy(WsdClient *cl)
{
	return cl->proxy_data.proxy ? u_strdup( cl->proxy_data.proxy ) : NULL;
}

void
wsdc_transport_set_userName(WsdClient * cl, const char *arg)
{
	u_free(cl->data.user);
	cl->data.user = arg ? u_strdup(arg) : NULL;
}

char *
wsdc_transport_get_userName(WsdClient * cl)
{
	return cl->data.user ? u_strdup(cl->data.user) : NULL;
}

void
wsdc_transport_set_password(WsdClient * cl, const char *arg)
{
	u_free(cl->data.pwd);
	cl->data.pwd = arg ? u_strdup(arg) : NULL;
}

char *
wsdc_transport_get_password(WsdClient * cl)
{
	return cl->data.pwd ? u_strdup(cl->data.pwd) : NULL;
}

void
wsdc_transport_set_proxyauth(WsdClient * cl, const char *arg)
{
	u_free(cl->proxy_data.proxy_auth);
	cl->proxy_data.proxy_auth = arg ? u_strdup( arg ) : NULL;
}

char *
wsdc_transport_get_proxyauth(WsdClient *cl)
{
	return cl->proxy_data.proxy_auth ? u_strdup( cl->proxy_data.proxy_auth ) : NULL;
}


unsigned long
wsdc_transport_get_timeout(WsdClient * cl)
{
	return cl->transport_timeout;
}

void
wsdc_transport_set_timeout(WsdClient * cl, unsigned long arg)
{
	cl->transport_timeout = arg;
}


char *
wsdc_transport_get_auth_method(WsdClient * cl)
{
  return cl->authentication.method ? u_strdup(cl->authentication.method) : NULL;
}

void
wsdc_transport_set_auth_method(WsdClient * cl, const char *arg)
{
	u_free(cl->authentication.method);
	cl->authentication.method = arg ? u_strdup( arg ) : NULL;
}

int
wsdc_is_auth_method(WsdClient * cl, int method)
{
	if (cl->authentication.method == NULL) {
		return 1;
	}
	if (method >= WS_MAX_AUTH) {
		return 0;
	}
	return (!strncasecmp
		(cl->authentication.method, auth_methods[method],
		 strlen(cl->authentication.method)));
}

wsdc_auth_type_t
wsdc_transport_get_auth_value(WsdClient * cl)
{
	char *m = cl->authentication.method;
	wsdc_auth_type_t i;

	if (m == NULL) {
		return WS_NO_AUTH;
	}
	for (i = 0; auth_methods[i] != NULL; i++) {
		if (!strcasecmp(m, auth_methods[i])) {
			return i;
		}
	}
	return WS_MAX_AUTH;
}


void
wsdc_transport_set_verify_peer(WsdClient * cl, unsigned int arg)
{
	cl->authentication.verify_peer = arg;
}

unsigned int
wsdc_transport_get_verify_peer(WsdClient *cl)
{
	return cl->authentication.verify_peer;
}


void
wsdc_transport_set_verify_host(WsdClient * cl, unsigned int arg)
{
	cl->authentication.verify_host = arg;
}

unsigned int
wsdc_transport_get_verify_host(WsdClient *cl)
{
	return cl->authentication.verify_host;
}

void
wsdc_transport_set_crlcheck(WsdClient * cl, unsigned int arg)
{
	cl->authentication.crl_check = arg;
}

unsigned int
wsdc_transport_get_crlcheck(WsdClient * cl)
{
        return cl->authentication.crl_check;
}

#ifndef _WIN32
void
wsdc_transport_set_crlfile(WsdClient * cl, const char *arg)
{
        u_free(cl->authentication.crl_file);
        cl->authentication.crl_file = arg ? u_strdup( arg ) : NULL;
}

char *
wsdc_transport_get_crlfile(WsdClient *cl)
{
  return cl->authentication.crl_file ? u_strdup(cl->authentication.crl_file) : NULL; 
}
#endif


void
wsdc_transport_set_cainfo(WsdClient * cl, const char *arg)
{
	u_free(cl->authentication.cainfo);
	cl->authentication.cainfo = arg ? u_strdup( arg ) : NULL;
}

char *
wsdc_transport_get_cainfo(WsdClient *cl)
{
	return cl->authentication.cainfo ? u_strdup( cl->authentication.cainfo ) : NULL;
}

static int
hexadecimal2raw(const char *str, unsigned char *dest, int len)
{
        int i = 0;
        unsigned char v1 =0 , v2 = 0;
        while(*str != 0 && *(str+1) != 0 && i < len) {
                if(*str <= '9' && *str >= '0')
                        v1 = *str - '0';
                else if(*str <= 'f' && *str >= 'a')
                        v1 = *str - 'a' + 10;
                else if(*str <= 'F' && *str >= 'A')
                        v1 = *str - 'A' + 10;
                v1 <<= 4;
                str++;
                if(*str <= '9' && *str >= '0')
                        v2 = *str - '0';
                else if(*str <= 'f' && *str >= 'a')
                        v2 = *str - 'a' + 10;
                else if(*str <= 'F' && *str >= 'A')
                        v2 = *str - 'A' + 10;
                str++;
                dest[i] = v1 + v2;
                i++;
        }
        return i;
}

static char *
raw2hexadecimal(unsigned char *dest, int len)
{
        int i = 0;
        char v;
        char * str = calloc(1, len*2+1);
        char * p = str;
        if(str == NULL) return str;
        while(i < len) {
                v = (dest[i] & 0xf0) >> 4;
                if(v <= 9)
                        *str = '0' + v;
                else
                        *str = 'a' + v - 10;
                str++;
                v = dest[i] & 0x0f;
                if(v <= 9)
                        *str = '0' + v;
                else
                        *str = 'a' + v - 10;
                str++;
                i++;
        }
        return p;
}


void
wsdc_transport_set_certhumbprint(WsdClient *cl, const char *arg)
{
	if(arg == NULL)
		return;
	hexadecimal2raw(arg, cl->authentication.certificatethumbprint, 20);
}

char *
wsdc_transport_get_certhumbprint(WsdClient *cl)
{
	return raw2hexadecimal(cl->authentication.certificatethumbprint, 20);
}

void
wsdc_transport_set_capath(WsdClient *cl, const char *arg)
{
	u_free(cl->authentication.capath);
	cl->authentication.capath = arg ? u_strdup( arg ) : NULL;
}

char *
wsdc_transport_get_capath(WsdClient *cl)
{
	return cl->authentication.capath ? u_strdup( cl->authentication.capath ) : NULL;
}


void
wsdc_transport_set_caoid(WsdClient *cl, const char *arg)
{
	u_free(cl->authentication.caoid);
	cl->authentication.caoid = arg ? u_strdup( arg ) : NULL;
}

char *
wsdc_transport_get_caoid(WsdClient *cl)
{
	return cl->authentication.caoid ? u_strdup( cl->authentication.caoid ) : NULL;
}


#ifdef _WIN32
void
wsdc_transport_set_calocal(WsdClient *cl, BOOL local)
{
	cl->authentication.calocal = local;
}

BOOL
wsdc_transport_get_calocal(WsdClient *cl)
{
	return cl->authentication.calocal;
}
#endif

char *
wsdc_transport_get_proxy_username(WsdClient *cl)
{
  return cl->proxy_data.proxy_username ? u_strdup(cl->proxy_data.proxy_username) : NULL;
}

void
wsdc_transport_set_proxy_username(WsdClient *cl, const char *proxy_username )
{
  u_free(cl->proxy_data.proxy_username);
  cl->proxy_data.proxy_username = proxy_username ? u_strdup(proxy_username) : NULL;
}

char *
wsdc_transport_get_proxy_password(WsdClient *cl)
{
  return cl->proxy_data.proxy_password ? u_strdup(cl->proxy_data.proxy_password) : NULL;
}

void
wsdc_transport_set_proxy_password(WsdClient *cl, const char *proxy_password )
{
  u_free(cl->proxy_data.proxy_password);
  cl->proxy_data.proxy_password = proxy_password ? u_strdup(proxy_password) : NULL;
}


void
wsdc_transport_set_cert(WsdClient * cl, const char *arg)
{
	u_free(cl->authentication.sslcert);
	cl->authentication.sslcert = arg ? u_strdup( arg ) : NULL;
}

char *
wsdc_transport_get_cert(WsdClient *cl)
{
	return cl->authentication.sslcert ? u_strdup( cl->authentication.sslcert ) : NULL;
}


void
wsdc_transport_set_key(WsdClient *cl, const char *key)
{
	u_free(cl->authentication.sslkey);
	cl->authentication.sslkey = key ? u_strdup( key ) : NULL;
}

char *
wsdc_transport_get_key(WsdClient *cl)
{
	return cl->authentication.sslkey ? u_strdup( cl->authentication.sslkey ) : NULL;
}


char *
wsd_transport_get_last_error_string(WS_LASTERR_Code err)
{
  switch (err) {
  case WS_LASTERR_OK:
    return "Everything OK";
  case WS_LASTERR_FAILED_INIT:
    return "Transport initialization failed";
  case WS_LASTERR_NO_REQUEST:
    return "Request is NULL";
  case WS_LASTERR_NO_CLIENT:
    return "No client associated with request";
  case WS_LASTERR_UNSUPPORTED_PROTOCOL:
    return "Unsupported protocol";
  case WS_LASTERR_URL_MALFORMAT:
    return "URL malformat";
  case WS_LASTERR_COULDNT_RESOLVE_PROXY:
    return "Could not resolve proxy";
  case WS_LASTERR_COULDNT_RESOLVE_HOST:
    return "Could not resolve host";
  case WS_LASTERR_COULDNT_CONNECT:
    return "Could not connect";
  case WS_LASTERR_HTTP_RETURNED_ERROR:
    return "HTTP returned error";
  case WS_LASTERR_WRITE_ERROR:
    return "Write error";
  case WS_LASTERR_READ_ERROR:
    return "Read error";
  case WS_LASTERR_OUT_OF_MEMORY:
    return "Could not alloc memory";
  case WS_LASTERR_OPERATION_TIMEOUTED:
    return "Operation timeout reached";
  case WS_LASTERR_HTTP_POST_ERROR:
    return "HTTP POST error";
  case WS_LASTERR_BAD_DOWNLOAD_RESUME:
    return "Could not resume download";
  case WS_LASTERR_TOO_MANY_REDIRECTS:
    return "Catch endless re-direct loop";
  case WS_LASTERR_SSL_CONNECT_ERROR:
    return "SSL connection error";
  case WS_LASTERR_SSL_PEER_CERTIFICATE:
    return "Peer's certificate wasn't OK";
  case WS_LASTERR_SSL_ENGINE_NOTFOUND:
    return "SSL crypto engine not found";
  case WS_LASTERR_SSL_ENGINE_SETFAILED:
    return "Can't set SSL crypto engine default";
  case WS_LASTERR_SSL_CERTPROBLEM:
    return "Problem with the local certificate";
  case WS_LASTERR_SSL_CACERT:
    return "Problem with the CA certificate";
  case WS_LASTERR_SSL_ENGINE_INITFAILED:
    return "Failed to initialise SSL engine";
  case WS_LASTERR_SEND_ERROR:
    return "Failed sending network data";
  case WS_LASTERR_RECV_ERROR:
    return "Failure in receiving network data";
  case WS_LASTERR_BAD_CONTENT_ENCODING:
    return "Unrecognized transfer encoding";
  case WS_LASTERR_LOGIN_DENIED:
    return "User, password or similar was not accepted";
  case WS_LASTERR_CURL_BAD_FUNCTION_ARG:
    return "Internal error: Wrong function argument in call to CURL";
  default:
    return "Unrecognized error";
  }
}
