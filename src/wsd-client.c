/*******************************************************************************
 * Copyright (C) 2004-2006 Intel Corp. All rights reserved.
 * Copyright (C) 2020 Klaus Kämpf <kkaempf@suse.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice,cl
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  - Neither the name of Intel Corp. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL Intel Corp. OR THE CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGclE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

/**
 * @author Anas Nashif
 * @author Klaus Kämpf
 */

#ifdef HAVE_CONFIG_H
#include <wsd_config.h>
#endif

#include <stdbool.h>

#include "u/libu.h"
#include "wsd-xml-api.h"
#include "wsd-soap.h"
#include "wsd-soap-envelope.h"

#include "wsd-xml.h"
#include "wsd-xml-binding.h"
#include "wsd-xml-serialize.h"
#include "wsd-client-transport.h"
#include "wsd-faults.h"
#include "wsd-client.h"
#include "wsd-client-api.h"

void
wsdc_set_dumpfile( WsdClient *cl, FILE *f )
{
  if (f)
    cl->dumpfile = f;
  return;
}


FILE *
wsdc_get_dumpfile(WsdClient *cl)
{
  return cl->dumpfile;
}


#ifndef _WIN32
void
wsdc_set_conffile(WsdClient *cl, char *f )
{
  u_free(cl->client_config_file);
  cl->client_config_file = (f != NULL) ? u_strdup(f): NULL;
}

char *
wsdc_get_conffile(WsdClient *cl)
{
        return cl->client_config_file;
}
#endif


static WsXmlDocH
wsdc_build_envelope(WsSerializerContextH serctx,
		const char *action,
		const char *reply_to_uri,
		const char *to_uri,
		request_opt_t *options)
{
	WsXmlNodeH      node;
	char            uuidBuf[100];
	WsXmlNodeH      header;
	WsXmlDocH       doc = ws_xml_create_envelope();
	if (!doc) {
		error("Error while creating envelope");
		return NULL;
	}

	header = ws_xml_get_soap_header(doc);
	if (header == NULL) {
		return NULL;
	}
	generate_uuid(uuidBuf, sizeof(uuidBuf), 0);

	if (reply_to_uri == NULL) {
		reply_to_uri = WSA_TO_ANONYMOUS;
	}
	if (to_uri == NULL) {
		to_uri = WSA_TO_ANONYMOUS;
	}
	if (action != NULL) {
		ws_serialize_str(serctx, header,
			(char *)action, XML_NS_ADDRESSING, WSA_ACTION, 1);
	}

	if (to_uri) {
		ws_serialize_str(serctx, header, (char *)to_uri,
			XML_NS_ADDRESSING, WSA_TO, 1);
	}
	if (uuidBuf[0] != 0) {
		ws_serialize_str(serctx, header, uuidBuf,
			XML_NS_ADDRESSING, WSA_MESSAGE_ID, 1);
	}
	if (options->timeout) {
	  /* FIXME: see wsd-xml-serialize.c */
		char buf[20];
		int ret;
		ret = snprintf(buf, sizeof(buf), "PT%u.%uS",
				(unsigned int) options->timeout / 1000,
				(unsigned int) options->timeout % 1000);
		if (ret < 0 || ret >= sizeof(buf)) {
			error("Error: formating time");
			return NULL;
		}
		ws_serialize_str(serctx, header, buf,
			XML_NS_WDP_SCAN, WSM_OPERATION_TIMEOUT, 0);
	}
	if (options->max_envelope_size) {
		ws_serialize_uint32(serctx, header, options->max_envelope_size,
				XML_NS_WDP_SCAN, WSM_MAX_ENVELOPE_SIZE,
				options->flags & FLAG_MUND_MAX_ESIZE);
	}
	if (options->fragment) {
          /* FIXME: mu set but unused
		int mu = 0;
		if ((options->flags & FLAG_MUND_FRAGMENT) ==
				FLAG_MUND_FRAGMENT)
			mu = 1;
           */
		ws_serialize_str(serctx, header, options->fragment,
				XML_NS_WDP_SCAN, WSM_FRAGMENT_TRANSFER,
				1);
	}
	if (options->locale) {
          node = ws_xml_add_child(header, XML_NS_WDP_SCAN, WSM_LOCALE, NULL);
          ws_xml_set_node_lang(node, options->locale);
          ws_xml_add_node_attr(node, XML_NS_SOAP_1_2, SOAP_MUST_UNDERSTAND, "false");
	}

	node = ws_xml_add_child(header, XML_NS_ADDRESSING, WSA_REPLY_TO, NULL);
	if (node == NULL) {
		return NULL;
	}
	ws_xml_add_child(node, XML_NS_ADDRESSING, WSA_ADDRESS, (char *)reply_to_uri);

	return doc;
}



// Access to request elements

WsXmlDocH
wsd_response_doc(WsdRequest *rq)
{
  return rq->response_doc;
}

/**
 * get request response node
 * @param rq WsdRequest
 * @return WsXmlNodeH
 */

WsXmlNodeH
wsd_response_node(WsdRequest *rq)
{
  return rq->node;
}


WS_LASTERR_Code
wsd_get_last_error(WsdRequest *rq)
{
  return rq->last_error;
}


long
wsd_get_response_code(WsdRequest *rq)
{
  return rq->response_code;
}

char *
wsd_get_fault_string(WsdRequest *rq)
{
  return rq->fault_string;
}

/* -------------------- client functions --------------------- */

WsSerializerContextH
wsdc_get_serialization_context(WsdClient * cl)
{
	return cl->serctx;
}

int
wsdc_in_debug_mode(WsdClient *cl)
{
    return (cl->flags & FLAG_DEBUG_MODE);
}

char *
wsdc_get_hostname(WsdClient * cl)
{
	return cl->data.hostname? u_strdup (cl->data.hostname) : NULL;
}


unsigned int
wsdc_get_port(WsdClient * cl)
{
	return cl->data.port;
}

char *
wsdc_get_scheme(WsdClient * cl)
{
	return cl->data.scheme ?  u_strdup ( cl->data.scheme) : NULL;
}


char *
wsdc_get_path(WsdClient * cl)
{
	return cl->data.path ?  u_strdup( cl->data.path ) : NULL;
}


char *
wsdc_get_user(WsdClient * cl)
{
	return  cl->data.user ? u_strdup( cl->data.user ) : NULL;
}


char *
wsdc_get_encoding(WsdClient *cl)
{
	return cl->content_encoding;
}


char *
wsdc_get_password(WsdClient * cl)
{
	return   cl->data.pwd ? u_strdup( cl->data.pwd ) : NULL;
}


char *
wsdc_get_endpoint(WsdClient * cl)
{
	return cl->data.endpoint ? cl->data.endpoint : NULL;
}




WsXmlDocH
wsdc_read_file( const char *filename,
		const char *encoding, unsigned long options)
{
	return ws_xml_read_file( filename, encoding, options);
}

WsXmlDocH
wsdc_read_memory( char *buf,
		size_t size, const char *encoding, unsigned long options)
{
	return ws_xml_read_memory( buf, size, encoding, options);
}

request_opt_t *
wsd_options_create(void)
{
  request_opt_t *op = u_malloc(sizeof(request_opt_t));
  if (op)
    memset(op, 0, sizeof(request_opt_t));
  else
    return NULL;
  return op;
}

void
wsd_options_destroy(request_opt_t * op)
{
  if (op->options) {
    hash_free(op->options);
  }
  u_free(op->fragment);
  u_free(op->delivery_uri);
  u_free(op->reference);
  u_free(op);
  return;
}

void
wsd_set_options_flags(request_opt_t * options, unsigned long flag)
{
  options->flags |= flag;
  return;
}


unsigned long
wsd_get_options_flags(request_opt_t * options)
{
  return options->flags;
}


void
wsd_clear_options_flags(request_opt_t * options, unsigned long flag)
{
  options->flags &= ~flag;
  return;
}


void
wsdc_set_fragment(const char *fragment, request_opt_t * options)
{
  u_free(options->fragment);
  options->fragment = fragment ? u_strdup(fragment) : NULL;
}

void
wsdc_set_reference(const char *reference, request_opt_t * options)
{
  u_free(options->reference);
  options->reference = reference ? u_strdup(reference) : NULL;
}

void
wsdc_set_sub_expiry(float event_subscription_expire, request_opt_t * options)
{
	options->expires = event_subscription_expire;
}

void
wsdc_set_heartbeat_interval(float heartbeat_interval, request_opt_t * options)
{
	options->heartbeat_interval = heartbeat_interval;
}

void
wsdc_set_locale(request_opt_t * options, const char *locale)
{
  u_free(options->locale);
  options->locale = locale ? u_strdup(locale) : NULL;
}

char *
wsdc_get_locale(request_opt_t * options)
{
  return options->locale;
}

void
wsdc_node_to_buf(WsXmlNodeH node, char **buf) {
	int   len;
	WsXmlDocH doc = ws_xml_create_doc_by_import( node);
	ws_xml_dump_memory_enc(doc, buf, &len, "UTF-8");
	ws_xml_destroy_doc(doc);
	return;
}


char*
wsdc_node_to_formatbuf(WsXmlNodeH node) {
	char *buf;
	int   len;
	WsXmlDocH doc = ws_xml_create_doc_by_import( node);
	ws_xml_dump_memory_node_tree(ws_xml_get_doc_root(doc), &buf, &len);
	ws_xml_destroy_doc(doc);
	return buf;
}


static void
add_subscription_context(WsXmlNodeH node, char *context)
{
	WsXmlNodeH subsnode;
	WsXmlDocH doc = ws_xml_read_memory(context, strlen(context), "UTF-8", 0);
	if(doc == NULL) return;
	subsnode = ws_xml_get_doc_root(doc);
	ws_xml_duplicate_children(node, subsnode);
}

/*
 * generate urn:uuid:...
 */

static char *
_gen_urn()
{
  /* urn:uuid:...37 chars... */
#define URN_BUF_SIZE SIZE_OF_UUID_STRING+4+5
  char *urn = u_malloc(URN_BUF_SIZE);
  if (!urn) {
    error("Can't allocate URN");
    return NULL;
  }
  snprintf(urn, 5, "urn:");
  /* urn+4 => urn+strlen("urn:") */
  if (!generate_uuid(urn+4, SIZE_OF_UUID_STRING+5, 0)) {
    error("generate_uuid() failed");
    return NULL;
  }
  return urn;
}


/*
 * add child with integer value as text
 */
static void
_add_int_child(WsXmlNodeH parent, const char *ns, const char *name, int value)
{
    char numbuf[16];
    snprintf(numbuf, 15, "%d", value);
    ws_xml_add_child(parent, ns, name, numbuf);
}


WsdRequest *
wsd_request_create(WsdClient *cl,
                    WsdAction action,
                    request_opt_t *options,
                    void *data)
{
  WsXmlNodeH body;
  WsXmlNodeH header;
  WsXmlNodeH node;
  WsdScanJob *scan_job;
  char       *_action = NULL;
  char       *urn;

  WsdRequest *request = (WsdRequest *) calloc(1, sizeof(WsdRequest));
  if (request == NULL) {
    error("Can't alloc WsdRequest");
    return NULL;
  }
  request->client = cl;
  request->action = action;
  request->request_doc = wsdc_build_envelope(cl->serctx, _action,
                                    WSA_TO_ANONYMOUS, cl->data.endpoint, options);
  request->response_code = 0;
  u_buf_create(&(request->response_buf));
  u_buf_create(&(request->request_buf));

  header = ws_xml_get_soap_header(request->request_doc);
  if (!header) {
    error("Failed to retrieve <header> from envelope");
    wsd_request_destroy(request);
    return NULL;
  }
    
  WsXmlNodeH from = ws_xml_add_child(header, XML_NS_ADDRESSING, WSA_FROM, NULL);
  ws_xml_add_child(from, XML_NS_ADDRESSING, WSA_ADDRESS, cl->from_urn);

  body = ws_xml_get_soap_body(request->request_doc);
  if (!body) {
    error("Failed to retrieve <body> from envelope");
    wsd_request_destroy(request);
    return NULL;
  }

  switch (action) {
  case WSD_ACTION_GET_SCANNER_ELEMENTS:
    ws_xml_add_child(header, XML_NS_ADDRESSING, WSA_ACTION, WSD_GET_SCANNER_ELEMENTS);
    if (data) {
        WsXmlNodeH req = ws_xml_add_child(body, XML_NS_WDP_SCAN, WSD_GET_SCANNER_ELEMENTS_REQUEST, NULL);
        WsXmlNodeH req_elements = ws_xml_add_child(req, XML_NS_WDP_SCAN, WSD_REQUESTED_ELEMENTS, NULL);
        ws_xml_add_child(req_elements, XML_NS_WDP_SCAN, WSD_NAME, (char *)data);
    }
    break;
  case WSD_ACTION_CREATE_SCAN_JOB:
    ws_xml_add_child(header, XML_NS_ADDRESSING, WSA_ACTION, WSD_CREATE_SCAN_JOB);
    if (!data) {
        static WsdScanOptions scan_options = {
            .jobname = "scanjob",
            .username = "sane",
            .format = "jfif",
            .images_to_transfer = 1,
            .input_source = "Platen",
            .content_type = "Auto",
            .front_color_mode = "RGB24",
            .back_color_mode = NULL,
            .x_resolution = 300,
            .y_resolution = 300,
            .auto_size = 0,
            .x_offset = 0,
            .y_offset = 0,
            .width = 595,
            .height = 595,
            .auto_exposure = 0,
            .brightness = 0, /* -1000...1000 */
            .contrast = 0, /* -1000...1000 */
            .sharpness = 0 /* -1000...1000 */
        };
        data = &scan_options;
    }
    WsdScanOptions *scan_options = (WsdScanOptions *)data;
    WsXmlNodeH req = ws_xml_add_child(body, XML_NS_WDP_SCAN, WSD_CREATE_SCAN_JOB_REQUEST, NULL);
    WsXmlNodeH scan_ticket = ws_xml_add_child(req, XML_NS_WDP_SCAN, WSD_SCAN_TICKET, NULL);
    WsXmlNodeH job_description = ws_xml_add_child(scan_ticket, XML_NS_WDP_SCAN, WSD_JOB_DESCRIPTION, NULL);
    ws_xml_add_child(job_description, XML_NS_WDP_SCAN, WSD_JOB_NAME, scan_options->jobname);
    ws_xml_add_child(job_description, XML_NS_WDP_SCAN, WSD_JOB_ORIGINATING_USER_NAME, scan_options->username);
      /* <wscn:DocumentParameters> */
    WsXmlNodeH document_parameters = ws_xml_add_child(scan_ticket, XML_NS_WDP_SCAN, WSD_DOCUMENT_PARAMETERS, NULL);
      /*    <wscn:Format> */
    ws_xml_add_child(document_parameters, XML_NS_WDP_SCAN, WSD_FORMAT, scan_options->format);
      /*    <wscn:ImagesToTransfer> */
    _add_int_child(document_parameters, XML_NS_WDP_SCAN, WSD_IMAGES_TO_TRANSFER, scan_options->images_to_transfer);
      /*    <wscn:InputSource> */
    ws_xml_add_child(document_parameters, XML_NS_WDP_SCAN, WSD_INPUT_SOURCE, scan_options->input_source);
      /*    <wscn:ContentType> */
    ws_xml_add_child(document_parameters, XML_NS_WDP_SCAN, WSD_CONTENT_TYPE, scan_options->content_type);
      /*    <wscn:InputSize> */
    WsXmlNodeH input_size = ws_xml_add_child(document_parameters, XML_NS_WDP_SCAN, WSD_INPUT_SIZE, NULL);
    if (scan_options->auto_size) {
        /*      <wscn:DocumentSizeAutoDetect> */
        ws_xml_add_child(input_size, XML_NS_WDP_SCAN, WSD_DOCUMENT_SIZE_AUTO_DETECT, "true");
    }
    else {
        /*      <wscn:DocumentSizeAutoDetect> */
        ws_xml_add_child(input_size, XML_NS_WDP_SCAN, WSD_DOCUMENT_SIZE_AUTO_DETECT, "false");
        /*    <wscn:InputMediaSize> */
        WsXmlNodeH input_media_size = ws_xml_add_child(document_parameters, XML_NS_WDP_SCAN, WSD_INPUT_MEDIA_SIZE, NULL);
        _add_int_child(input_media_size, XML_NS_WDP_SCAN, WSD_WIDTH, scan_options->width);
        _add_int_child(input_media_size, XML_NS_WDP_SCAN, WSD_HEIGHT, scan_options->height);
    }
      /*    <wscn:Exposure> */
    WsXmlNodeH exposure = ws_xml_add_child(document_parameters, XML_NS_WDP_SCAN, WSD_EXPOSURE, NULL);
    if (scan_options->auto_exposure) {
        /*    <wscn:AutoExposure> */
        ws_xml_add_child(exposure, XML_NS_WDP_SCAN, WSD_AUTO_EXPOSURE, "true");
    }
    else {
        /*    <wscn:ExposureSettings> */
        WsXmlNodeH exposure_settings = ws_xml_add_child(exposure, XML_NS_WDP_SCAN, WSD_EXPOSURE_SETTINGS, NULL);
        /*      <wscn:Brightness> */
        _add_int_child(exposure_settings, XML_NS_WDP_SCAN, WSD_BRIGHTNESS, scan_options->brightness);
        /*      <wscn:Contrast> */
        _add_int_child(exposure_settings, XML_NS_WDP_SCAN, WSD_CONTRAST, scan_options->contrast);
        /*      <wscn:Sharpness> */
        _add_int_child(exposure_settings, XML_NS_WDP_SCAN, WSD_SHARPNESS, scan_options->sharpness);
    }
      /*    <wscn:MediaSides> */
    WsXmlNodeH media_sides = ws_xml_add_child(document_parameters, XML_NS_WDP_SCAN, WSD_MEDIA_SIDES, NULL);
    if (scan_options->front_color_mode) {
        WsXmlNodeH media_front = ws_xml_add_child(media_sides, XML_NS_WDP_SCAN, WSD_MEDIA_FRONT, NULL);
        ws_xml_add_child(media_front, XML_NS_WDP_SCAN, WSD_COLOR_PROCESSING, scan_options->front_color_mode);
        WsXmlNodeH resolution = ws_xml_add_child(media_front, XML_NS_WDP_SCAN, WSD_RESOLUTION, NULL);
        _add_int_child(resolution, XML_NS_WDP_SCAN, WSD_WIDTH, scan_options->x_resolution);
        _add_int_child(resolution, XML_NS_WDP_SCAN, WSD_HEIGHT, scan_options->y_resolution);
    }
    break;
  case WSD_ACTION_RETRIEVE_IMAGE:
    ws_xml_add_child(header, XML_NS_ADDRESSING, WSA_ACTION, WSD_RETRIEVE_IMAGE);
    scan_job = (WsdScanJob *)data;
    req = ws_xml_add_child(body, XML_NS_WDP_SCAN, WSD_RETRIEVE_IMAGE_REQUEST, NULL);
    ws_xml_add_child(req, XML_NS_WDP_SCAN, WSD_JOB_ID, scan_job->id);
    ws_xml_add_child(req, XML_NS_WDP_SCAN, WSD_JOB_TOKEN, scan_job->token);
    WsXmlNodeH document_description = ws_xml_add_child(req, XML_NS_WDP_SCAN, WSD_DOCUMENT_DESCRIPTION, NULL);
    ws_xml_add_child(document_description, XML_NS_WDP_SCAN, WSD_DOCUMENT_NAME, scan_job->document_name);
    break;
  case WSD_ACTION_CANCEL_JOB:
    ws_xml_add_child(header, XML_NS_ADDRESSING, WSA_ACTION, WSD_CANCEL_SCAN_JOB);
    req = ws_xml_add_child(body, XML_NS_WDP_SCAN, WSD_CANCEL_JOB_REQUEST, NULL);
    scan_job = (WsdScanJob *)data;
    ws_xml_add_child(req, XML_NS_WDP_SCAN, WSD_JOB_ID, scan_job->id);
    break;
#if 0
  case WSD_ACTION_VALIDATE_SCAN_TICKET:
    _action = "ValidateScanTicket";
    break;
  case WSD_ACTION_GET_JOB_ELEMENTS:
    _action = "GetJobElements";
    break;
  case WSD_ACTION_GET_ACTIVE_JOBS:
    _action = "GetActiveJobs";
    break;
  case WSD_ACTION_GET_JOB_HISTORY:
    _action = "GetJobHistory";
    break;
#endif
  default:
    error("wsd_request_create(): unknown action");
    return NULL;
    break;
  }
  
  if (options && (options->flags & FLAG_DUMP_REQUEST) == FLAG_DUMP_REQUEST) {
    ws_xml_dump_node_tree(cl->dumpfile, ws_xml_get_doc_root(request->request_doc));
  }
  return request;
}



static WsXmlDocH
_wsd_action_create(WsdClient * cl,
		WsdAction action,
		void *data,
		void *typeInfo,
		request_opt_t *options)
{
  WsXmlDocH response;
  WsdRequest *request = wsd_request_create(cl, action, options, data);
  if (!request)
          return NULL;
  
  if (wsd_send_request(request, NULL)) {
    ws_xml_destroy_doc(request->request_doc);
    return NULL;
  }
  response = wsd_build_envelope_from_response(request, NULL);
  ws_xml_destroy_doc(request->request_doc);
  return response;
}

WsXmlDocH
wsd_action_create(WsdClient * cl,
		WsdAction action,
		request_opt_t *options,
		WsXmlDocH source_doc)
{
  return _wsd_action_create(cl, action, source_doc, NULL, options);
}


WsXmlDocH
wsd_action_create_fromtext(WsdClient * cl,
		WsdAction action,
		request_opt_t *options,
		const char *data, size_t size, const char *encoding)
{
  WsXmlDocH source_doc = wsdc_read_memory( (char *)data, size, (char *)encoding, 0);
  WsXmlDocH response;
  if (source_doc == NULL) {
    error("could not convert XML text to doc");
    return NULL;
  }

  response = _wsd_action_create(cl, action, source_doc, NULL, options);
  ws_xml_destroy_doc(source_doc);
  return response;
}

WsXmlDocH
wsd_action_create_serialized(WsdClient * cl,
		WsdAction action,
		request_opt_t *options,
		void *data,
		void *typeInfo)
{
  return _wsd_action_create(cl, action, data, typeInfo, options);
}


WsdRequest *
wsd_action_get_scanner_element(WsdClient * cl, request_opt_t *options, char *element)
{
  WsXmlDocH response;
  WsXmlDocH doc;
  
  WsdRequest *request = wsd_request_create(cl, WSD_ACTION_GET_SCANNER_ELEMENTS, options, element);
  if (!request)
    return NULL;

  if (!strcmp(cl->data.scheme, "file")) {
    cl->flags |= FLAG_DEBUG_MODE;
    doc = wsdc_read_file(cl->data.path, "UTF-8", 0);
  }
  else {
    cl->flags &= ~FLAG_DEBUG_MODE;
    if (wsd_send_request(request, NULL)) {
      return NULL;
    }
    doc = request->response_doc;
  }
    if (options && (options->flags & FLAG_DUMP_RESPONSE) == FLAG_DUMP_RESPONSE) {
        ws_xml_dump_node_tree(cl->dumpfile, ws_xml_get_doc_root(doc));
    }
  WsXmlNodeH scanner_elements = ws_xml_get_xpath_node(doc, "/soap:Envelope/soap:Body/sca:GetScannerElementsResponse/sca:ScannerElements");
  if (scanner_elements) {
    int count = ws_xml_get_child_count_by_qname(scanner_elements, XML_NS_WDP_SCAN, WSD_ELEMENT_DATA);

    for (int i = 0; i < count; i++) {
      WsXmlAttrH attr;
      WsXmlNodeH element_data = ws_xml_get_child(scanner_elements, i, XML_NS_WDP_SCAN, WSD_ELEMENT_DATA);
      /*         <sca:ElementData Name="sca:ScannerStatus" Valid="true"> */
      attr = ws_xml_find_node_attr(element_data, NULL, WSD_VALID);
      if (!attr) {
        error("<sca:ElementData> without Valid attribute");
        continue;
      }
      char *valid = ws_xml_get_attr_value(attr);
      attr = ws_xml_find_node_attr(element_data, NULL, WSD_NAME);
      if (!attr) {
        error("<sca:ElementData> without NAME attribute");
        continue;
      }
      char *name = ws_xml_get_attr_value(attr);
      if (strcmp(valid, "true")) {
        error("ElementData %s is not valid, skipping", name);
        continue;
      }
      char *colon = strchr(name, ':');
      if (colon)
        name = colon+1;
      /* get first child */
      request->node = ws_xml_get_child(element_data, 0, XML_NS_WDP_SCAN, name);
    }
  }
  return request;
}

/**
 * GetScannerElements:ScannerDescription
 */
WsdRequest *
wsd_action_get_scanner_description(WsdClient * cl, request_opt_t *options)
{
  debug("wsd_action_get_scanner_description()");
  return wsd_action_get_scanner_element(cl, options, WSD_SCANNER_DESCRIPTION);
}

/**
 * GetScannerElements:ScannerConfiguration
 */
WsdRequest *
wsd_action_get_scanner_configuration(WsdClient * cl, request_opt_t *options)
{
  debug("wsd_action_get_scanner_configuration()");
  return wsd_action_get_scanner_element(cl, options, WSD_SCANNER_CONFIGURATION);
}

/**
 * GetScannerElements:ScannerStatus
 */
WsdRequest *
wsd_action_get_scanner_status(WsdClient * cl, request_opt_t *options)
{
  debug("wsd_action_get_scanner_status()");
  return wsd_action_get_scanner_element(cl, options, WSD_SCANNER_STATUS);
}


/*
 * create scan job
 *
 */
WsdRequest *
wsd_action_create_scan_job(WsdClient * cl, request_opt_t *options, WsdScanOptions *scan_options)
{
    WsXmlDocH response;
    debug("wsd_action_create_scan_job()");
    WsdRequest *request = wsd_request_create(cl, WSD_ACTION_CREATE_SCAN_JOB, options, scan_options);
    if (!request)
        return NULL;
    if (wsdc_in_debug_mode(cl))
        return request;
    if (wsd_send_request(request, NULL)) {
        wsd_request_destroy(request);
        return NULL;
    }
    WsXmlDocH doc = request->response_doc;
    if (!doc) {
        error("No response document after %s\n", WSD_CREATE_SCAN_JOB);
        wsd_request_destroy(request);
        return NULL;
    }
    if (options && (options->flags & FLAG_DUMP_RESPONSE) == FLAG_DUMP_RESPONSE) {
        ws_xml_dump_node_tree(cl->dumpfile, ws_xml_get_doc_root(doc));
    }
    WsXmlNodeH body = ws_xml_get_soap_body(doc);
    if (!body) {
        error("No response body after %s\n", WSD_CREATE_SCAN_JOB);
        wsd_request_destroy(request);
        return NULL;
    }
    WsXmlNodeH scan_job_response = ws_xml_find_in_tree(body, XML_NS_WDP_SCAN, WSD_CREATE_SCAN_JOB_RESPONSE, 1);
    if (!scan_job_response) {
        error("No %s in %s response\n", WSD_CREATE_SCAN_JOB_RESPONSE, WSD_CREATE_SCAN_JOB);
        wsd_request_destroy(request);
        return NULL;
    }
    request->node = scan_job_response;
    return request;
}


WsdRequest *
wsd_action_retrieve_image(WsdClient * cl, request_opt_t *options, WsdScanJob *scan_job, u_buf_t **image_buf)
{
    WsXmlDocH response;
    debug("wsd_action_retrieve_image()");
    WsdRequest *request = wsd_request_create(cl, WSD_ACTION_RETRIEVE_IMAGE, options, scan_job);
    if (!request)
        return NULL;
    if (wsd_send_request(request, image_buf)) {
        wsd_request_destroy(request);
        return NULL;
    }
    WsXmlDocH doc = request->response_doc;
    if (!doc) {
        error("No response document after %s\n", WSD_RETRIEVE_IMAGE);
        wsd_request_destroy(request);
        return NULL;
    }
    if (options && (options->flags & FLAG_DUMP_RESPONSE) == FLAG_DUMP_RESPONSE) {
        ws_xml_dump_node_tree(cl->dumpfile, ws_xml_get_doc_root(doc));
    }
    WsXmlNodeH body = ws_xml_get_soap_body(doc);
    if (!body) {
        error("No response body after %s\n", WSD_RETRIEVE_IMAGE);
        wsd_request_destroy(request);
        return NULL;
    }
    WsXmlNodeH retrieve_image_response = ws_xml_find_in_tree(body, XML_NS_WDP_SCAN, WSD_RETRIEVE_IMAGE_RESPONSE, 1);
    if (!retrieve_image_response) {
        error("No %s in %s response\n", WSD_RETRIEVE_IMAGE_RESPONSE, WSD_RETRIEVE_IMAGE);
        wsd_request_destroy(request);
        return NULL;
    }
    request->node = retrieve_image_response;
    return request;
}

/**
 * Cancel scan job
 */
WsdRequest *
wsd_action_cancel_scan_job(WsdClient * cl, request_opt_t *options, WsdScanJob *scan_job)
{
    WsdRequest *request = wsd_request_create(cl, WSD_ACTION_CANCEL_JOB, options, scan_job);
    if (!request)
        return NULL;
    if (wsd_send_request(request, NULL)) {
        wsd_request_destroy(request);
        return NULL;
    }
    return request;
}


/**
 * Build Inbound Envelope from Response
 * @param cl Client Handler
 * @return XML document with Envelope
 */

WsXmlDocH
wsd_build_envelope_from_response(WsdRequest *rq, u_buf_t **image_buf)
{
  WsXmlDocH       doc = NULL;
  WsdClient *cl = rq->client;
  u_buf_t        *buffer = rq->response_buf;

  if (!buffer || !u_buf_ptr(buffer)) {
    error("NULL response");
    return NULL;
  }
  char *start = u_buf_ptr(buffer);
  unsigned int size = u_buf_len(buffer);
/*fwrite(start, 1, size, stdout);*/
  if (image_buf) {
      char *boundary = NULL;
      char *pivot = start;
      char *xml_start = NULL;
      u_buf_create(image_buf);

      while (size > 0) {
          pivot = strchr(pivot, 0x0d);
          if (!pivot) {
              break;
          }
          if (*(pivot+1) == 0x0a) {
              *pivot = 0x00;
          }
          if (!boundary) {
              boundary = strdup(start);
          }
          else if (start == pivot) {
              if (doc == NULL) {
                  /* end of html header */
                  xml_start = start + 2;
              }
              else {
                  /* end of SOAP */
                  u_buf_append(*image_buf, start+2, size-2); /* skip 0x0d, 0x0a */
                  break;
              }
          }
          else if (!strcmp(boundary, start)) {
              /* end of SOAP body */
              free(boundary);
              doc = ws_xml_read_memory (xml_start, start-xml_start, cl->content_encoding, 0);
          }
          *pivot = 0x0d;
          pivot += 2;
          size -= (pivot - start);
          start = pivot;
      }
  }
  else {
      doc = ws_xml_read_memory( start, size, cl->content_encoding, 0);
  }
  if (doc == NULL) {
    error("could not create xmldoc from response");
  }
  return doc;
}


WsdClient*
wsd_client_create_from_url(const char* endpoint)
{
	u_uri_t *uri = NULL;
	WsdClient* cl;
	if (!endpoint) {
		return NULL;
	}
	if (u_uri_parse((const char *) endpoint, &uri) != 0 ) {
		return NULL;
	}
	if (!uri) {
		return NULL;
	}
	cl = wsd_client_create( uri->host,
			uri->port,
			uri->path,
			uri->scheme,
			uri->user,
			uri->pwd);
	u_uri_free(uri);
	return cl;
}

int
wsdc_set_encoding(WsdClient *cl, const char *encoding)
{
	u_free(cl->content_encoding);
	cl->content_encoding = encoding ? u_strdup(encoding) : NULL;
	return 0;
}

int
wsdc_check_for_fault(WsXmlDocH doc)
{
	return wsd_is_fault_envelope(doc);
}


WsdFault *
wsdc_fault_new(void)
{
	WsdFault     *fault =
		(WsdFault *) u_zalloc(sizeof(WsdFault));

	if (fault)
		return fault;
	else
		return NULL;
}

void
wsdc_fault_destroy(WsdFault *fault)
{
	/*
	if (fault->code)
		u_free(fault->code);
	if (fault->subcode)
		u_free(fault->subcode);
	if (fault->reason)
		u_free(fault->reason);
	if (fault->fault_detail)
		u_free(fault->fault_detail);
		*/
	u_free(fault);
	return;
}



void
wsdc_get_fault_data(WsXmlDocH doc,
		WsdFault *fault)
{
	WsXmlNodeH body;
	WsXmlNodeH fault_node;
	WsXmlNodeH code;
	WsXmlNodeH reason;
	WsXmlNodeH detail;
	if (wsdc_check_for_fault(doc) == 0 || !fault )
		return;

	body = ws_xml_get_soap_body(doc);
	fault_node = ws_xml_get_child(body, 0, XML_NS_SOAP_1_2, SOAP_FAULT);
	if (!fault_node)
		return;

	code = ws_xml_get_child(fault_node, 0, XML_NS_SOAP_1_2, SOAP_CODE);
	if (code) {
		WsXmlNodeH code_v = ws_xml_get_child(code, 0 , XML_NS_SOAP_1_2, SOAP_VALUE);
		WsXmlNodeH subcode = ws_xml_get_child(code, 0 , XML_NS_SOAP_1_2, SOAP_SUBCODE);
		WsXmlNodeH subcode_v = ws_xml_get_child(subcode, 0 , XML_NS_SOAP_1_2, SOAP_VALUE);
		fault->code = ws_xml_get_node_text(code_v);
		fault->subcode = ws_xml_get_node_text(subcode_v);
	}
	reason = ws_xml_get_child(fault_node, 0, XML_NS_SOAP_1_2, SOAP_REASON);
	if (reason) {
		WsXmlNodeH reason_text = ws_xml_get_child(reason, 0 , XML_NS_SOAP_1_2, SOAP_TEXT);
		fault->reason = ws_xml_get_node_text(reason_text);
	}
	detail = ws_xml_get_child(fault_node, 0, XML_NS_SOAP_1_2, SOAP_DETAIL);
	if (detail) {
		// FIXME
		// WsXmlNodeH fault_detail = ws_xml_get_child(detail, 0 , XML_NS_WDP_SCAN, SOAP_FAULT_DETAIL);
		// fault->fault_detail = ws_xml_get_node_text(fault_detail);
		fault->fault_detail = ws_xml_get_node_text(detail);
	}
	return;
}



WsdClient *
wsd_client_create(const char *hostname,
		const int port,
		const char *path,
		const char *scheme,
		const char *username,
		const char *password)
{
#ifndef _WIN32
        dictionary *ini;
#endif
	WsdClient *client = (WsdClient *) calloc(1, sizeof(WsdClient));
        if (client == NULL) {
          error("Can't alloc WsdClient");
          return NULL;
        }
	client->hdl = &client->data;

#ifndef _WIN32
	wsdc_set_conffile(client, DEFAULT_CONFIG_FILE);
        ini = iniparser_new(wsdc_get_conffile(client));
        if (ini) {
          char *user_agent = iniparser_getstr(ini, "client:agent");
          if (user_agent) {
            wsdc_transport_set_agent(client, user_agent);
          }
          iniparser_free(ini);
        }
#endif
        client->from_urn = _gen_urn();
	client->serctx = ws_serializer_init();
	client->dumpfile = stdout;
	client->data.scheme = u_strdup(scheme ? scheme : "http");
	client->data.hostname = hostname ? u_strdup(hostname) : u_strdup("localhost");
	client->data.port = port;
	client->data.path = u_strdup(path ? path : "/ws");
	client->data.user = username ? u_strdup(username) : NULL;
	client->data.pwd = password ? u_strdup(password) : NULL;
	client->data.auth_set = 0;
	client->initialized = 0;
	client->transport_timeout = 0;
  client->content_encoding = u_strdup("UTF-8");
#ifdef _WIN32
	client->session_handle = 0;
#endif
	client->data.endpoint = u_strdup_printf("%s://%s:%d%s%s",
                                             client->data.scheme, client->data.hostname,
                                             client->data.port,
                                             (*client->data.path == '/') ? "" : "/",
                                             client->data.path);
	debug("Endpoint: %s", client->data.endpoint);
	client->authentication.verify_host = 1; //verify CN in server certicates by default
	client->authentication.verify_peer = 1; //validate server certificates by default
#ifndef _WIN32
	client->authentication.crl_check = 0;   // No CRL check by default
	client->authentication.crl_file = NULL;
#endif

	return client;
}


/*
 * destroy WsdRequest
 */

void
wsd_request_destroy(WsdRequest *rq)
{
  if (rq->fault_string) {
    u_free(rq->fault_string);
  }
  if (rq->request_doc) {
    ws_xml_destroy_doc(rq->request_doc);
  }
  if (rq->request_buf) {
    u_buf_free(rq->request_buf);
  }
  if (rq->response_doc) {
    ws_xml_destroy_doc(rq->response_doc);
  }
  if (rq->response_buf) {
    u_buf_free(rq->response_buf);
  }

  u_free(rq);
}


void
wsd_client_destroy(WsdClient * cl)
{
#ifndef _WIN32
  if (cl->client_config_file) {
    u_free(cl->client_config_file);
  }
#endif
  if (cl->data.scheme) {
    u_free(cl->data.scheme);
  }
  if (cl->data.hostname) {
    u_free(cl->data.hostname);
  }
  if (cl->data.path) {
    u_free(cl->data.path);
  }
  if (cl->data.user) {
    u_free(cl->data.user);
  }
  if (cl->data.pwd) {
    u_free(cl->data.pwd);
  }
  if (cl->data.endpoint) {
    u_free(cl->data.endpoint);
  }
  if (cl->from_urn) {
    u_free(cl->from_urn);
  }
  if (cl->serctx) {
    ws_serializer_cleanup(cl->serctx);
  }
  if (cl->content_encoding) {
    u_free(cl->content_encoding);
  }
  if (cl->authentication.crl_file != NULL) {
    u_free(cl->authentication.crl_file);
  }
  if (cl->authentication.method != NULL) {
    u_free(cl->authentication.method);
  }
  if (cl->authentication.cainfo != NULL) {	 
    u_free(cl->authentication.cainfo);
  }
  if (cl->authentication.caoid != NULL) {
    u_free(cl->authentication.caoid);
  }
  if (cl->proxy_data.proxy_username != NULL) {
    u_free(cl->proxy_data.proxy_username);
  }
  if (cl->proxy_data.proxy_password != NULL) {
    u_free(cl->proxy_data.proxy_password);
  }

  wsdc_transport_close_transport(cl);

  u_free(cl);
}


void
wsdc_remove_query_string(const char *s, char **result)
{
  char           *r = 0;
  const char     *q;
  char           *buf = 0;

  buf = u_strndup(s, strlen(s));
  if (!buf) {
    *result = NULL;
    return;
  }
  if ((q = strchr(buf, '?')) != NULL) {
    r = u_strndup(s, q - buf);
    *result = r;
  }
  else {
    *result = (char *)s;
  }

  u_free(buf);
}
