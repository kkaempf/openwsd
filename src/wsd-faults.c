/*******************************************************************************
 * Copyright (C) 2004-2006 Intel Corp. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  - Neither the name of Intel Corp. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL Intel Corp. OR THE CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

/**
 * @author Anas Nashif
 */
#ifdef HAVE_CONFIG_H
#include <wsd_config.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#include <u/libu.h>
#include "wsd-xml-api.h"
#include "wsd-client-api.h"
#include "wsd-soap.h"

#include "wsd-xml.h"
#include "wsd-xml-serializer.h"
#include "wsd-soap-envelope.h"
#include "wsd-soap-message.h"
#include "wsd-faults.h"


#define FAULT_XPATH_EXPR  "/s:Envelope/s:Body/s:Fault/s:Code/s:Value"
#define FAULT_SUBCODE_VALUE_XPATH "/s:Envelope/s:Body/s:Fault/s:Code/s:Subcode/s:Value"

WsdFaultDetailTable
fault_detail_table[] =
{
	{ WSD_DETAIL_OK, NULL },
	{ WSD_DETAIL_ACK, "Ack" },
	{ WSD_DETAIL_ACTION_MISMATCH, "ActionMismatch" },
	{ WSD_DETAIL_ALREADY_EXISTS, "AlreadyExists" },
	{ WSD_DETAIL_AMBIGUOUS_SELECTORS, "AmbigousSelectors" },
	{ WSD_DETAIL_ASYNCHRONOUS_REQUEST, "AsynchronousRequest" },
	{ WSD_DETAIL_ADDRESSING_MODE, "AddressingMode" },
	{ WSD_DETAIL_AUTHERIZATION_MODE, "AutherizationMode" },
	{ WSD_DETAIL_BOOKMARKS, "Bookmarks" },
	{ WSD_DETAIL_CHARECHTER_SET, "CharechterSet" },
	{ WSD_DETAIL_DELIVERY_RETRIES, "DeliveryRetries" },
	{ WSD_DETAIL_DUPLICATE_SELECTORS, "DuplicateSelectors" },
	{ WSD_DETAIL_ENCODING_TYPE, "EncodingType" },
	{ WSD_DETAIL_ENUMERATION_MODE, "EnumerationMode" },
	{ WSD_DETAIL_EXPIRATION_TIME, "ExpirationTime" },
	{ WSD_DETAIL_EXPIRED, "Expired" },
	{ WSD_DETAIL_FILTERING_REQUIRED, "FilteringRequired" },
	{ WSD_DETAIL_FORMAT_MISMATCH, "FormatMismatch" },
	{ WSD_DETAIL_FORMAT_SECURITY_TOKEN, "FormatSecurityTocken" },
	{ WSD_DETAIL_FRAGMENT_LEVEL_ACCESS, "FragmentLevelAccess" },
	{ WSD_DETAIL_HEARTBEATS, "Heartbeats" },
	{ WSD_DETAIL_INSECURE_ADDRESS, "InsecureAddress" },
	{ WSD_DETAIL_INSUFFICIENT_SELECTORS, "InsufficientSelectors" },
	{ WSD_DETAIL_INVALID, "Invalid" },
	{ WSD_DETAIL_INVALID_ADDRESS, "InvalidAddress" },
	{ WSD_DETAIL_INVALID_FORMAT, "InvalidFormat" },
	{ WSD_DETAIL_INVALID_FRAGMENT, "InvalidFragment" },
	{ WSD_DETAIL_INVALID_NAME, "InvalidName" },
	{ WSD_DETAIL_INVALID_NAMESPACE, "InvalidNamespace" },
	{ WSD_DETAIL_INVALID_RESOURCEURI, "InvalidResourceURI" },
	{ WSD_DETAIL_INVALID_SELECTOR_ASSIGNMENT, "InvalidSelectorAssignment" },
	{ WSD_DETAIL_INVALID_SYSTEM, "InvalidSystem" },
	{ WSD_DETAIL_INVALID_TIMEOUT, "InvalidTimeout" },
	{ WSD_DETAIL_INVALID_VALUE, "InvalidValue" },
	{ WSD_DETAIL_INVALID_VALUES, "InvalidValues" },
	{ WSD_DETAIL_LOCALE, "Locale" },
	{ WSD_DETAIL_MAX_ELEMENTS, "MaxElements" },
	{ WSD_DETAIL_MAX_ENVELOPE_POLICY, "MaxEnvelopePolicy" },
	{ WSD_DETAIL_MAX_ENVELOPE_SIZE, "MaxEnvelopeSize" },
	{ WSD_DETAIL_MAX_TIME, "MaxTime" },
	{ WSD_DETAIL_MINIMUM_ENVELOPE_LIMIT, "MinimumEnvelopeLimit" },
	{ WSD_DETAIL_MISSING_VALUES, "MissingValues" },
	{ WSD_DETAIL_NOT_SUPPORTED, "NotSupported" },
	{ WSD_DETAIL_OPERATION_TIMEOUT, "OperationTimeout" },
	{ WSD_DETAIL_OPTION_LIMIT, "OptionLimit" },
	{ WSD_DETAIL_OPTION_SET, "OptionSet" },
	{ WSD_DETAIL_READ_ONLY, "ReadOnly" },
	{ WSD_DETAIL_RESOURCE_OFFLINE, "ResourceOffline" },
	{ WSD_DETAIL_RENAME, "Rename" },
	{ WSD_DETAIL_SELECTOR_LIMIT, "SelectorLimit" },
	{ WSD_DETAIL_SERVICE_ENVELOPE_LIMIT, "ServiceEnvelopeLimit" },
	{ WSD_DETAIL_TARGET_ALREADY_EXISTS, "TargetAlreadyExists" },
	{ WSD_DETAIL_TYPE_MISMATCH, "TypeMismatch" },
	{ WSD_DETAIL_UNEXPECTED_SELECTORS, "UnexpectedSelectors" },
	{ WSD_DETAIL_UNREPORTABLE_SUCCESS, "UnreportableSuccess" },
	{ WSD_DETAIL_UNUSABLE_ADDRESS, "UnusableAddress" },
	{ WSD_DETAIL_URI_LIMIT_EXCEEDED, "UriLimitExceeded" },
	{ WSD_DETAIL_WHITESPACE, "Whitespace" },

	// WS-Addressing
	{ WSA_DETAIL_DUPLICATE_MESSAGE_ID, "DuplicateMessageID" },

	// SOAP
	{ SOAP_DETAIL_HEADER_NOT_UNDERSTOOD, "HeaderNotUnderstood" },

	// WS-Trust
	{WST_DETAIL_UNSUPPORTED_TOKENTYPE, "UnsupportedTokenType"},

	// WS_Policy
	{WSP_DETAIL_INVALID_EPR, "InvalidEPR"},

	// OpenWSD
	{ WSD_NO_DETAILS, "Unknown" },
	{ WSD_SYSTEM_ERROR, "Unknown" }
};


int
wsd_is_fault_envelope( WsXmlDocH doc )
{
	WsXmlNodeH node = ws_xml_get_child(ws_xml_get_soap_body(doc),
			0 , XML_NS_SOAP_1_2 , SOAP_FAULT);
	if ( node != NULL )
		return 1;
	else
		return 0;
}

WsdKnownStatusCode
wsd_find_httpcode_for_value( WsXmlDocH doc )
{
	WsdKnownStatusCode httpcode = 200;
	char *xp = ws_xml_get_xpath_value(doc, FAULT_XPATH_EXPR );
	if (xp != NULL) {
		if (strcmp(xp, FAULT_RECEIVER_CODE_NS) == 0 )
			httpcode = WSD_STATUS_INTERNAL_SERVER_ERROR;
		else if (strcmp(xp, FAULT_SENDER_CODE_NS) == 0 )
			httpcode = WSD_STATUS_BAD_REQUEST;
	}
	u_free(xp);
	return httpcode;
}


int
wsd_fault_occured(WsdMessage *msg)
{
	return (msg->status.fault_code == WSD_RC_OK ) ?  0 : 1;
}


void
wsd_set_fault( WsdMessage *msg,
               WsdFaultCodeType fault_code,
               WsdFaultDetailType fault_detail_code,
               const char *details)
{
  if (!wsd_fault_occured(msg)) {
    msg->status.fault_code = fault_code;
    msg->status.fault_detail_code = fault_detail_code;
    if (details) msg->status.fault_msg = strdup(details);
  }
  return;
}


void
wsd_get_fault_status_from_doc (WsXmlDocH doc, WsdStatus *status)
{
  int i;
  char *subcode_value = ws_xml_get_xpath_value(doc, FAULT_SUBCODE_VALUE_XPATH);
  char *subcode_value_msg;
  char *start_pos;

  if (!subcode_value || strlen(subcode_value) == 0)
    return;

  subcode_value_msg = calloc(1, strlen(subcode_value));
  if (subcode_value_msg == NULL) {
    error("Out of memory");
    status->fault_code = WSD_INTERNAL_ERROR;
    /* some default values */
    status->fault_detail_code = WSD_SYSTEM_ERROR;
    status->fault_msg = NULL;
    return;
  }

  start_pos = strchr(subcode_value, ':');
  if (start_pos != NULL) {
    strcpy(subcode_value_msg, start_pos+1); 
  }
  free(subcode_value_msg);
  return;
}

