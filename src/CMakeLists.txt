#
# CMakeLists.txt for openwsd/src
#

add_subdirectory(test)

INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/include ${CMAKE_SOURCE_DIR} ${CMAKE_BINARY_DIR} )

SET( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${XML_CFLAGS} -g -DIGNORE_DUPLICATE_ID" )

SET( UTIL_SOURCES u/buf.c u/log.c u/memory.c u/misc.c  u/uri.c  u/uuid.c u/lock.c u/md5.c u/strings.c u/list.c u/hash.c u/base64.c u/iniparser.c u/debug.c u/uerr.c u/uoption.c u/gettimeofday.c u/syslog.c u/pthreadx_win32.c u/os.c )

SET( WSD_SOURCES ${UTIL_SOURCES}
                 wsd-client.c
                 wsd-client-transport.c
                 wsd-curl-client-transport.c
                 wsd-debug.c
                 wsd-faults.c
                 wsd-libxml2-binding.c
                 wsd-soap.c
                 wsd-soap-envelope.c
                 wsd-soap-message.c
                 wsd-xml.c wsd-xml-serialize.c)


ADD_LIBRARY( openwsd SHARED ${WSD_SOURCES} )

TARGET_LINK_LIBRARIES( openwsd ${LIBXML2_LIBRARIES} )
TARGET_LINK_LIBRARIES( openwsd ${CMAKE_THREAD_LIBS_INIT} )
if( HAVE_LIBDL )
TARGET_LINK_LIBRARIES( openwsd ${DL_LIBRARIES})
endif( HAVE_LIBDL )

SET_TARGET_PROPERTIES( openwsd PROPERTIES VERSION 1.0.0 SOVERSION 1)
INSTALL(TARGETS openwsd DESTINATION ${LIB_INSTALL_DIR})
