/*******************************************************************************
* Copyright (C) 2004-2006 Intel Corp. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
*  - Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
*  - Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
*  - Neither the name of Intel Corp. nor the names of its
*    contributors may be used to endorse or promote products derived from this
*    software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL Intel Corp. OR THE CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/**
 * @author Vadim Revyakin
 * @author Anas Nashif
 */

#ifndef WSDC_CLIENT_TRANSPORT_H_
#define WSDC_CLIENT_TRANSPORT_H_

#ifdef _WIN32
#include <windows.h> /* for BOOL */
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include "wsd-client-api.h"


#ifdef PACKAGE_STRING
#define DEFAULT_USER_AGENT PACKAGE_STRING
#else
#define DEFAULT_USER_AGENT "OpenWsd"
#endif

#define _WS_NO_AUTH "No Auth"
#define _WS_BASIC_AUTH "Basic"
#define _WS_DIGEST_AUTH "Digest"
#define _WS_PASS_AUTH "Passport"
#define _WS_NTLM_AUTH "NTLM"
#define _WS_GSSNEGOTIATE_AUTH "GSS-Negotiate"

int wsd_send_request(WsdRequest *request, u_buf_t **image_buf);

/*
 * Set callback function to ask for username/password on authentication failure (http-401 returned)
 * If the callback returns an empty (or NULL) username, authentication is aborted.
 */

extern void wsdc_transport_set_auth_request_func(WsdClient *cl,
        wsdc_auth_request_func_t f);

extern int wsdc_is_auth_method(WsdClient *cl, int method);

extern int wsd_transport_init(WsdClient *cl, void *arg);

extern void wsdc_transport_close_transport(WsdClient *cl);

extern void wsdc_transport_fini(WsdClient *cl);

extern void   wsdc_transport_set_agent(WsdClient *cl, const char *agent);
extern char * wsdc_transport_get_agent (WsdClient *cl);

extern void wsdc_transport_set_userName(WsdClient *cl, const char *user_name);
extern char *wsdc_transport_get_userName(WsdClient *cl);
extern void wsdc_transport_set_password(WsdClient *cl, const char *password);
extern char *wsdc_transport_get_password(WsdClient *cl);

extern void wsdc_transport_set_proxy_username(WsdClient *cl, const char *proxy_username );
extern char *wsdc_transport_get_proxy_username(WsdClient *cl );
extern void wsdc_transport_set_proxy_password(WsdClient *cl, const char *proxy_password );
extern char *wsdc_transport_get_proxy_password(WsdClient *cl );

extern void   wsdc_transport_set_auth_method(WsdClient *cl, const char *am);
extern char * wsdc_transport_get_auth_method (WsdClient *cl);

extern const char *wsdc_transport_get_auth_name(wsdc_auth_type_t auth);

extern  wsdc_auth_type_t wsdc_transport_get_auth_value(WsdClient *cl);

char *wsdc_transport_get_last_error_string(WS_LASTERR_Code err);

extern void          wsdc_transport_set_timeout(WsdClient *cl, unsigned long timeout);
extern unsigned long wsdc_transport_get_timeout(WsdClient *cl);

extern void wsdc_transport_set_verify_peer(WsdClient *cl, unsigned int value);
extern unsigned int  wsdc_transport_get_verify_peer(WsdClient *cl);

extern void wsdc_transport_set_verify_host(WsdClient *cl, unsigned int value);
extern unsigned int  wsdc_transport_get_verify_host(WsdClient *cl);

extern void wsdc_transport_set_crlcheck(WsdClient * cl, unsigned int value);
extern unsigned int wsdc_transport_get_crlcheck(WsdClient * cl);

extern void wsdc_transport_set_crlfile(WsdClient *cl, const char *arg);
extern char *wsdc_transport_get_crlfile(WsdClient *cl);

extern void  wsdc_transport_set_proxy(WsdClient *cl, const char *proxy);
extern char *wsdc_transport_get_proxy(WsdClient *cl);

extern void  wsdc_transport_set_proxyauth(WsdClient *cl, const char *pauth);
extern char *wsdc_transport_get_proxyauth(WsdClient *cl);

extern void  wsdc_transport_set_cainfo(WsdClient *cl, const char *cainfo);
extern char *wsdc_transport_get_cainfo(WsdClient *cl);

extern void wsdc_transport_set_certhumbprint(WsdClient *cl, const char *arg);
extern char *wsdc_transport_get_certhumbprint(WsdClient *cl);

extern void  wsdc_transport_set_capath(WsdClient *cl, const char *capath);
extern char *wsdc_transport_get_capath(WsdClient *cl);

extern void  wsdc_transport_set_caoid(WsdClient *cl, const char *oid);
extern char *wsdc_transport_get_caoid(WsdClient *cl);

#ifdef _WIN32
extern void wsdc_transport_set_calocal(WsdClient *cl, BOOL local);
extern BOOL wsdc_transport_get_calocal(WsdClient *cl);
#endif

extern void  wsdc_transport_set_cert(WsdClient *cl, const char *cert);
extern char *wsdc_transport_get_cert(WsdClient *cl);

extern void  wsdc_transport_set_key(WsdClient *cl, const char *key);
extern char *wsdc_transport_get_key(WsdClient *cl);

#ifdef DEBUG_VERBOSE
long long get_transfer_time(void);
#endif

#ifdef __cplusplus
}
#endif

#endif  /* WSDC_CLIENT_TRANSPORT_H_ */

