/*******************************************************************************
 * Copyright (C) 2004-2006 Intel Corp. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  - Neither the name of Intel Corp. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL Intel Corp. OR THE CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

/**
 * @author Anas Nashif
 * @author Vadim Revyakin
 * @author Sumeet Kukreja, Dell Inc.
 */

#ifndef _WSMAN_NAMESPACES_H_
#define _WSMAN_NAMESPACES_H_

// NameSpaces
#define XML_NS_SOAP_1_1             "http://schemas.xmlsoap.org/soap/envelope"
#define XML_NS_SOAP_1_2             "http://www.w3.org/2003/05/soap-envelope"


#define XML_NS_XML_NAMESPACES       "http://www.w3.org/XML/1998/namespace"
#define XML_NS_XML_SCHEMA           "http://www.w3.org/2001/XMLSchema"

#define XML_NS_ADDRESSING           "http://schemas.xmlsoap.org/ws/2004/08/addressing"
#define XML_NS_DEVPROF              "http://schemas.xmlsoap.org/ws/2006/02/devprof"
#define XML_NS_DISCOVERY            "http://schemas.xmlsoap.org/ws/2005/04/discovery"
#define XML_NS_ENVELOPE             "http://www.w3.org/2003/05/soap-envelope"
#define XML_NS_EVENTING             "http://schemas.xmlsoap.org/ws/2004/08/eventing"
#define XML_NS_ENUMERATION          "http://schemas.xmlsoap.org/ws/2004/09/enumeration"
#define XML_NS_MEX                  "http://schemas.xmlsoap.org/ws/2004/09/mex"
#define XML_NS_TRANSFER             "http://schemas.xmlsoap.org/ws/2004/09/transfer"
#define XML_NS_XML_SCHEMA           "http://www.w3.org/2001/XMLSchema"
#define XML_NS_SCHEMA_INSTANCE      "http://www.w3.org/2001/XMLSchema-instance"
#define XML_NS_PNPX                 "http://schemas.microsoft.com/windows/pnpx/2005/10"
#define XML_NS_POLICY		    "http://schemas.xmlsoap.org/ws/2004/09/policy"
#define XML_NS_TRUST		    "http://schemas.xmlsoap.org/ws/2005/02/trust"
#define XML_NS_SE		    "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
#define XML_NS_WDP_PRINT            "http://schemas.microsoft.com/windows/2006/08/wdp/print"
#define XML_NS_WDP_SCAN             "http://schemas.microsoft.com/windows/2006/08/wdp/scan"
#define XML_NS_XOP                  "http://www.w3.org/2004/08/xop/include"

#define XML_NS_SCHEMA_INSTANCE_PREFIX "xsi"
#define XML_NS_SCHEMA_INSTANCE_NIL  "nil"

#define XML_NS_OPENWSD              "http://schema.openwsd.org/2019/openwsd"
        
// Actions

#define WSD_GET_SCANNER_ELEMENTS "http://schemas.microsoft.com/windows/2006/08/wdp/scan/GetScannerElements"
#define WSD_CREATE_SCAN_JOB "http://schemas.microsoft.com/windows/2006/08/wdp/scan/CreateScanJob"
#define WSD_RETRIEVE_IMAGE "http://schemas.microsoft.com/windows/2006/08/wdp/scan/RetrieveImage"
#define WSD_CANCEL_SCAN_JOB "http://schemas.microsoft.com/windows/2006/08/wdp/scan/CancelJob"

// GetScannerElementsRequest
#define WSD_GET_SCANNER_ELEMENTS_REQUEST "GetScannerElementsRequest"

#define WSD_ELEMENT_DATA          "ElementData"
#define WSD_REQUESTED_ELEMENTS    "RequestedElements"
#define WSD_NAME                  "Name"
#define WSD_COMPONENT             "Component"
#define WSD_SEVERITY              "Severity"
#define WSD_VALID                 "Valid"

// ScannerElements
#define WSD_SCANNER_ELEMENTS      "ScannerElements"
#define WSD_SCANNER_DESCRIPTION   "ScannerDescription"
#define WSD_SCANNER_STATUS        "ScannerStatus"
#define WSD_SCANNER_STATE         "ScannerState"
#define WSD_SCANNER_STATE_REASONS "ScannerStateReasons"
#define WSD_SCANNER_STATE_REASON  "ScannerStateReason"
#define WSD_SCANNER_NAME          "ScannerName"
#define WSD_SCANNER_INFO          "ScannerInfo"
#define WSD_SCANNER_LOCATION      "ScannerLocation"
#define WSD_SCANNER_CONFIGURATION "ScannerConfiguration"

// ScannerStatus elements
#define WSD_SCANNER_ACTIVE_CONDITIONS "ActiveConditions"
#define WSD_SCANNER_DEVICE_CONDITION "DeviceCondition"

// ScannerState
#define WSD_SCANNER_STATE_IDLE          "Idle"
#define WSD_SCANNER_STATE_PROCESSING "Processing"
// DeviceSettings
#define WSD_DEVICE_SETTINGS       "DeviceSettings"
#define WSD_FORMATS_SUPPORTED     "FormatsSupported"
#define WSD_COMPRESSION_QUALITY_FACTOR_SUPPORTED "CompressionQualityFactorSupported"
#define WSD_MIN_VALUE             "MinValue"
#define WSD_MAX_VALUE             "MaxValue"
#define WSD_FORMAT_VALUE          "FormatValue"
#define WSD_CONTENT_TYPES_SUPPORTED "ContentTypesSupported"
#define WSD_CONTENT_TYPE_VALUE    "ContentTypeValue"
#define WSD_DOCUMENT_SIZE_AUTO_DETECT_SUPPORTED "DocumentSizeAutoDetectSupported"
#define WSD_AUTO_EXPOSURE_SUPPORTED "AutoExposureSupported"
#define WSD_BRIGHTNESS_SUPPORTED  "BrightnessSupported"
#define WSD_CONTRAST_SUPPORTED    "ContrastSupported"
#define WSD_SHARPNESS_SUPPORTED    "SharpnessSupported"
#define WSD_SCALING_RANGE_SUPPORTED "ScalingRangeSupported"
#define WSD_SCALING_WIDTH         "ScalingWidth"
#define WSD_SCALING_HEIGHT        "ScalingHeight"
#define WSD_ROTATIONS_SUPPORTED   "RotationsSupported"
#define WSD_ROTATION_VALUE        "RotationValue"

// Platen
#define WSD_PLATEN                "Platen"
#define WSD_PLATEN_OPTICAL_RESOLUTION "PlatenOpticalResolution"
#define WSD_PLATEN_RESOLUTIONS    "PlatenResolutions"
#define WSD_WIDTH                 "Width"
#define WSD_HEIGHT                "Height"
#define WSD_WIDTHS                "Widths"
#define WSD_HEIGHTS               "Heights"
#define WSD_PLATEN_COLOR          "PlatenColor"
#define WSD_COLOR_ENTRY           "ColorEntry"
#define WSD_PLATEN_MINIMUM_SIZE   "PlatenMinimumSize"
#define WSD_PLATEN_MAXIMUM_SIZE   "PlatenMaximumSize"

// ADF
#define WSD_ADF                   "ADF"
#define WSD_ADF_DUPLEX            "ADFDuplex"
#define WSD_ADF_SUPPORTS_DUPLEX   "ADFSupportsDuplex"
#define WSD_ADF_FRONT             "ADFFront"
#define WSD_ADF_BACK              "ADFBack"
#define WSD_ADF_OPTICAL_RESOLUTION "ADFOpticalResolution"
#define WSD_ADF_RESOLUTIONS       "ADFResolutions"
#define WSD_ADF_COLOR             "ADFColor"
#define WSD_ADF_MINIMUM_SIZE      "ADFMinimumSize"
#define WSD_ADF_MAXIMUM_SIZE      "ADFMaximumSize"

// Film
#define WSD_FILM                  "Film"
#define WSD_FILM_OPTICAL_RESOLUTION "FilmOpticalResolution"
#define WSD_FILM_RESOLUTIONS      "FilmResolutions"
#define WSD_FILM_COLOR            "FilmColor"

// Color
#define WSD_COLOR_ENTRY_BW1       "BlackAndWhite1"
#define WSD_COLOR_ENTRY_GS4       "Grayscale4"
#define WSD_COLOR_ENTRY_GS8       "Grayscale8"
#define WSD_COLOR_ENTRY_GS16      "Grayscale16"
#define WSD_COLOR_ENTRY_RGB24     "RGB24"
#define WSD_COLOR_ENTRY_RGB48     "RGB48"
#define WSD_COLOR_ENTRY_RGBA32    "RGBa32"
#define WSD_COLOR_ENTRY_RGBA64    "RGBa64"

// Job
#define WSD_CREATE_SCAN_JOB_REQUEST  "CreateScanJobRequest"
#define WSD_CREATE_SCAN_JOB_RESPONSE "CreateScanJobResponse"
#define WSD_CANCEL_JOB_REQUEST       "CancelJobRequest"

#define WSD_SCAN_TICKET              "ScanTicket"
#define WSD_JOB_DESCRIPTION          "JobDescription"
#define WSD_JOB_NAME                 "JobName"
#define WSD_JOB_ORIGINATING_USER_NAME "JobOriginatingUserName"
#define WSD_DOCUMENT_PARAMETERS      "DocumentParameters"
#define WSD_IMAGES_TO_TRANSFER       "ImagesToTransfer"
#define WSD_FORMAT                   "Format"
#define WSD_INPUT_SOURCE             "InputSource"
#define WSD_CONTENT_TYPE             "ContentType"
#define WSD_INPUT_SIZE               "InputSize"
#define WSD_DOCUMENT_SIZE_AUTO_DETECT "DocumentSizeAutoDetect"
#define WSD_MEDIA_SIDES              "MediaSides"
#define WSD_MEDIA_FRONT              "MediaFront"
#define WSD_MEDIA_BACK               "MediaBack"
#define WSD_COLOR_PROCESSING         "ColorProcessing"
#define WSD_RESOLUTION               "Resolution"
#define WSD_JOB_ID                   "JobId"
#define WSD_JOB_TOKEN                "JobToken"
#define WSD_MEDIA_FRONT_IMAGE_INFO   "MediaFrontImageInfo"
#define WSD_PIXELS_PER_LINE          "PixelsPerLine"
#define WSD_NUMBER_OF_LINES          "NumberOfLines"
#define WSD_EXPOSURE                 "Exposure"
#define WSD_AUTO_EXPOSURE            "AutoExposure"
#define WSD_EXPOSURE_SETTINGS        "ExposureSettings"
#define WSD_BRIGHTNESS               "Brightness"
#define WSD_CONTRAST                 "Contrast"
#define WSD_SHARPNESS                "Sharpness"
#define WSD_INPUT_MEDIA_SIZE         "InputMediaSize"

// RetrieveImage
#define WSD_RETRIEVE_IMAGE_REQUEST   "RetrieveImageRequest"
#define WSD_RETRIEVE_IMAGE_RESPONSE  "RetrieveImageResponse"
#define WSD_DOCUMENT_DESCRIPTION     "DocumentDescription"
#define WSD_DOCUMENT_NAME            "DocumentName"
#define WSD_SCAN_DATA                "ScanData"
#define WSD_XOP_INCLUDE              "Include"
#define WSD_XOP_HREF                 "href"
#define WSD_XOP_CID                  "cid:"
#define WSD_XOP_CID_LEN              4

// SOAP
#define SOAP1_2_CONTENT_TYPE        "application/soap+xml;charset=UTF-8"
#define SOAP_CONTENT_TYPE           "application/soap+xml"
#define SOAP_ENCODING               "utf-8"
#define SOAP_SKIP_DEF_FILTERS       0x01
#define SOAP_ACTION_PREFIX       0x02 // otherwise exact
#define SOAP_ONE_WAY_OP             0x04
#define SOAP_NO_RESP_OP             0x08
#define SOAP_DONT_KEEP_INDOC        0x10

#define SOAP_CLIENT_RESPONSE        0x20 // internal use
#define SOAP_CUSTOM_DISPATCHER      0x40 // internal use
#define SOAP_IDENTIFY_DISPATCH      0x80 // internal use


#define WSMID_IDENTIFY              "Identify"
#define WSMID_IDENTIFY_RESPONSE     "IdentifyResponse"
#define WSMID_PROTOCOL_VERSION      "ProtocolVersion"
#define WSMID_PRODUCT_VENDOR        "ProductVendor"
#define WSMID_PRODUCT_VERSION       "ProductVersion"


#define XML_SCHEMA_NIL              "nil"


#define WSA_TO_ANONYMOUS     \
        "http://schemas.xmlsoap.org/ws/2004/08/addressing/role/anonymous"
#define WSA_MESSAGE_ID              "MessageID"
#define WSA_ADDRESS                 "Address"
#define WSA_EPR                     "EndpointReference"
#define WSA_ACTION                  "Action"
#define WSA_RELATES_TO              "RelatesTo"
#define WSA_TO                      "To"
#define WSA_REPLY_TO                "ReplyTo"
#define WSA_FROM                    "From"
#define WSA_FAULT_TO                "FaultTo"
#define WSA_REFERENCE_PROPERTIES    "ReferenceProperties"
#define WSA_REFERENCE_PARAMETERS    "ReferenceParameters"
#define WSA_ACTION_FAULT    \
         "http://schemas.xmlsoap.org/ws/2004/08/addressing/fault"



#define SOAP_ENVELOPE               "Envelope"
#define SOAP_HEADER                 "Header"
#define SOAP_BODY                   "Body"
#define SOAP_FAULT                  "Fault"
#define SOAP_CODE                   "Code"
#define SOAP_VALUE                  "Value"
#define SOAP_SUBCODE                "Subcode"
#define SOAP_REASON                 "Reason"
#define SOAP_TEXT                   "Text"
#define SOAP_LANG                   "lang"
#define SOAP_DETAIL                 "Detail"
#define SOAP_FAULT_DETAIL           "FaultDetail"
#define SOAP_MUST_UNDERSTAND        "mustUnderstand"
#define SOAP_VERSION_MISMATCH       "VersionMismatch"
#define SOAP_UPGRADE                "Upgrade"
#define SOAP_SUPPORTED_ENVELOPE     "SupportedEnvelope"



            /* ACTIONs */

#define WSD_OP_CREATE_SCAN_JOB      "CreateScanJob"
#define WSD_OP_RETRIEVE_IMAGE       "RetrieveImage"
#define WSD_OP_VALIDATE_SCAN_TICKET "ValidateScanTicket"
#define WSD_OP_GET_SCANNER_ELEMENTS "GetScannerElements"
#define WSD_OP_GET_JOB_ELEMENTS     "GetJobElements"
#define WSD_OP_GET_ACTIVE_JOBS      "GetActiveJobs"
#define WSD_OP_GET_JOB_HISTORY      "GetJobHistory"

#define WSENUM_ACTION_FAULT     \
           "http://schemas.xmlsoap.org/ws/2004/09/enumeration/fault"

#define WSMB_ASSOCIATED_INSTANCES   "AssociatedInstances"
#define WSMB_ASSOCIATION_INSTANCES  "AssociationInstances"
#define WSMB_OBJECT                 "Object"
#define WSMB_ASSOCIATION_CLASS_NAME "AssociationClassName"
#define WSMB_RESULT_CLASS_NAME      "ResultClassName"
#define WSMB_ROLE                   "Role"
#define WSMB_RESULT_ROLE            "ResultRole"
#define WSMB_INCLUDE_RESULT_PROPERTY "IncludeResultProperty"



#define WSM_SYSTEM                     "System"
#define WSM_LOCALE                     "Locale"
#define WSM_RESOURCE_URI               "ResourceURI"
#define WSM_SELECTOR_SET               "SelectorSet"
#define WSM_SELECTOR                   "Selector"
#define WSM_NAME                       "Name"
#define WSM_REQUEST_TOTAL              "RequestTotalItemsCountEstimate"
#define WSM_TOTAL_ESTIMATE             "TotalItemsCountEstimate"
#define WSM_OPTIMIZE_ENUM              "OptimizeEnumeration"
#define WSM_MAX_ELEMENTS               "MaxElements"
#define WSM_ENUM_EPR                   "EnumerateEPR"
#define WSM_ENUM_OBJ_AND_EPR           "EnumerateObjectAndEPR"
#define WSM_ENUM_MODE                  "EnumerationMode"
#define WSM_ITEM                       "Item"
#define WSM_FRAGMENT_TRANSFER          "FragmentTransfer"
#define WSM_XML_FRAGMENT               "XmlFragment"
#define WSM_OPTION_SET             	"OptionSet"
#define WSM_OPTION             		"Option"
#define WSM_TOTAL				"Total"
#define WSM_HEARTBEATS			"Heartbeats"
#define WSM_EVENTS				"Events"
#define WSM_ACTION				"Action"
#define WSM_EVENT				"Event"
#define WSM_ACKREQUESTED		"AckRequested"

#define WSM_MAX_ENVELOPE_SIZE           "MaxEnvelopeSize"
#define WSM_OPERATION_TIMEOUT           "OperationTimeout"
#define WSM_FAULT_SUBCODE               "FaultSubCode"
#define WSM_FILTER                      "Filter"
#define WSM_DIALECT                     "Dialect"
#define WSM_CONTENTCODING		"ContentEncoding"
#define WSM_CONNECTIONRETRY	"ConnectionRetry"
#define WSM_SENDBOOKMARKS	"SendBookmarks"
#define WSM_BOOKMARK			"Bookmark"
#define WSM_DROPPEDEVENTS		"DroppedEvents"
#define WSM_AUTH				"Auth"
#define WSM_PROFILE			"Profile"
#define WSM_CERTIFICATETHUMBPRINT		"CertificateThumbprint"

#define WXF_RESOURCE_CREATED                      "ResourceCreated"


// WSMB - Binding
#define WSMB_POLYMORPHISM_MODE          "PolymorphismMode"
#define WSMB_INCLUDE_SUBCLASS_PROP      "IncludeSubClassProperties"
#define WSMB_EXCLUDE_SUBCLASS_PROP      "ExcludeSubClassProperties"
#define WSMB_NONE      "None"
#define WSMB_DERIVED_REPRESENTATION     "DerivedRepresentation"
#define WSMB_SHOW_EXTENSION		"ShowExtensions"

/* ows:ExcludeNilProperties
   a per-request version of cim:omit_schema_optional in openwsman.conf
 */
#define WSMB_EXCLUDE_NIL_PROPS          "ExcludeNilProperties"


#define WSFW_RESPONSE_STR              "Response"
#define WSFW_INDOC                     "indoc"

#define WSFW_ENUM_PREFIX               "_en."

#define WST_ISSUEDTOKENS		    "IssuedTokens"
#define WST_REQUESTSECURITYTOKENRESPONSE    "RequestSecurityTokenResponse"
#define WST_TOKENTYPE		            "TokenType"
#define WST_REQUESTEDSECURITYTOKEN	    "RequestedSecurityToken"
#define WSSE_USERNAMETOKEN		    "UsernameToken"
#define WSSE_USERNAME			    "Username"
#define WSSE_PASSWORD			    "Password"
#define WSP_APPLIESTO  	                    "AppliesTo"

#define WST_USERNAMETOKEN \
	"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#UsernameToken"


#endif

