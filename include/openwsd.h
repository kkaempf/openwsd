#ifndef OPENWSD_H
#define OPENWSD_H

#include <openwsd/u/uuid.h>
#include <assert.h>
#include <openwsd/wsd-debug.h>
#include <openwsd/wsd-xml.h>
#include <openwsd/wsd-xml-binding.h>
#include <openwsd/wsd-names.h>
#include <openwsd/wsd-client-api.h>

#endif OPENWSD_H
