/*******************************************************************************
 * Copyright (C) 2004-2006 Intel Corp. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  - Neither the name of Intel Corp. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL Intel Corp. OR THE CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

/**
 * @author Anas Nashif
 */

#ifndef WSD_FAULTS_H_
#define WSD_FAULTS_H_

#define FAULT_SENDER_CODE "Sender"
#define FAULT_MUSTUNDERSTAND_CODE "MustUnderstand"
#define FAULT_RECEIVER_CODE "Receiver"
#define FAULT_SENDER_CODE_NS "s:Sender"
#define FAULT_RECEIVER_CODE_NS "s:Receiver"
#define FAULT_MUSTUNDERSTAND_CODE_NS "s:MustUnderstand"

enum __WsdFaultCodeType
{
    WSD_RC_OK = 0,
    WSD_ACCESS_DENIED,

    /** wsa:ActionNotSupported */
    WSA_ACTION_NOT_SUPPORTED,

    WSD_ALREADY_EXISTS,
    /** wsen:CannotProcessFilter */
    WSEN_CANNOT_PROCESS_FILTER,

    WSD_CANNOT_PROCESS_FILTER,
    WSD_CONCURRENCY,
    /**wse:DeliveryModeRequestedUnavailable */
    WSE_DELIVERY_MODE_REQUESTED_UNAVAILABLE,

    WSD_DELIVERY_REFUSED,
    /** wsa:DestinationUnreachable */
    WSA_DESTINATION_UNREACHABLE,

    WSD_ENCODING_LIMIT,
    /** wsa:EndpointUnavailable */
    WSA_ENDPOINT_UNAVAILABLE,

    WSD_EVENT_DELIVER_TO_UNUSABLE,

    /** wse:EventSourceUnableToProcess*/
    WSE_EVENT_SOURCE_UNABLE_TO_PROCESS,

    /** wsen:FilterDialectRequestedUnavailable */
    WSEN_FILTER_DIALECT_REQUESTED_UNAVAILABLE,

    /** wse:FilteringNotSupported */
    WSE_FILTERING_NOT_SUPPORTED,
    /** wsen:FilteringNotSupported */
    WSEN_FILTERING_NOT_SUPPORTED,
    /** wse:FilteringRequestedUnavailable */
    WSE_FILTERING_REQUESTED_UNAVAILABLE,

    WSD_FRAGMENT_DIALECT_NOT_SUPPORTED,
    WSD_INTERNAL_ERROR,
    WSD_INVALID_BOOKMARK,

    /** wsa:InvalidMessageInformationHeader */
    WSA_INVALID_MESSAGE_INFORMATION_HEADER,


    WSD_INVALID_OPTIONS,
    WSD_INVALID_PARAMETER,
    // WS-Transfer
    WXF_INVALID_REPRESENTATION,

    WSD_INVALID_SELECTORS,
    /** wsa:MessageInformationHeaderRequired */
    WSA_MESSAGE_INFORMATION_HEADER_REQUIRED,

    WSD_NO_ACK,
    WSD_QUOTA_LIMIT,
    WSD_SCHEMA_VALIDATION_ERROR,

    /** wsen:TimedOut */
    WSEN_TIMED_OUT,
    WSD_TIMED_OUT,

    /** wse:UnableToRenew */
    WSE_UNABLE_TO_RENEW,

    /** wse:UnsupportedExpirationType */
    WSE_UNSUPPORTED_EXPIRATION_TYPE,

    /** wsen:UnsupportedExpirationType */
    WSEN_UNSUPPORTED_EXPIRATION_TYPE,

    WSD_UNSUPPORTED_FEATURE,

    // SOAP
    SOAP_FAULT_VERSION_MISMATCH,
    // MustUnderstand
    SOAP_FAULT_MUSTUNDERSTAND,

    // wsmb:PolymorphismModeNotSupported
    WSMB_POLYMORPHISM_MODE_NOT_SUPPORTED,

    WSD_UNKNOWN
};
typedef enum  __WsdFaultCodeType WsdFaultCodeType;

enum __WsdFaultDetailType
{
    WSD_DETAIL_OK = 0,
    WSD_DETAIL_ACK,
    WSD_DETAIL_ACTION_MISMATCH,
    WSD_DETAIL_ALREADY_EXISTS,
    WSD_DETAIL_AMBIGUOUS_SELECTORS,
    WSD_DETAIL_ASYNCHRONOUS_REQUEST,
    WSD_DETAIL_ADDRESSING_MODE,
    WSD_DETAIL_AUTHERIZATION_MODE,
    WSD_DETAIL_BOOKMARKS,
    WSD_DETAIL_CHARECHTER_SET,
    WSD_DETAIL_DELIVERY_RETRIES,
    WSD_DETAIL_DUPLICATE_SELECTORS,
    WSD_DETAIL_ENCODING_TYPE,
    WSD_DETAIL_ENUMERATION_MODE,
    WSD_DETAIL_EXPIRATION_TIME,
    WSD_DETAIL_EXPIRED,
    WSD_DETAIL_FILTERING_REQUIRED,
    WSD_DETAIL_FORMAT_MISMATCH,
    WSD_DETAIL_FORMAT_SECURITY_TOKEN,
    WSD_DETAIL_FRAGMENT_LEVEL_ACCESS,
    WSD_DETAIL_HEARTBEATS,
    WSD_DETAIL_INSECURE_ADDRESS,
    WSD_DETAIL_INSUFFICIENT_SELECTORS,
    WSD_DETAIL_INVALID,
    WSD_DETAIL_INVALID_ADDRESS,
    WSD_DETAIL_INVALID_FORMAT,
    WSD_DETAIL_INVALID_FRAGMENT,
    WSD_DETAIL_INVALID_NAME,
    WSD_DETAIL_INVALID_NAMESPACE,
    WSD_DETAIL_INVALID_RESOURCEURI,
    WSD_DETAIL_INVALID_SELECTOR_ASSIGNMENT,
    WSD_DETAIL_INVALID_SYSTEM,
    WSD_DETAIL_INVALID_TIMEOUT,
    WSD_DETAIL_INVALID_VALUE,
    WSD_DETAIL_INVALID_VALUES,
    WSD_DETAIL_LOCALE,
    WSD_DETAIL_MAX_ELEMENTS,
    WSD_DETAIL_MAX_ENVELOPE_POLICY,
    WSD_DETAIL_MAX_ENVELOPE_SIZE,
    WSD_DETAIL_MAX_TIME,
    WSD_DETAIL_MINIMUM_ENVELOPE_LIMIT,
    WSD_DETAIL_MISSING_VALUES,
    WSD_DETAIL_NOT_SUPPORTED,
    WSD_DETAIL_OPERATION_TIMEOUT,
    WSD_DETAIL_OPTION_LIMIT,
    WSD_DETAIL_OPTION_SET,
    WSD_DETAIL_READ_ONLY,
    WSD_DETAIL_RESOURCE_OFFLINE,
    WSD_DETAIL_RENAME,
    WSD_DETAIL_SELECTOR_LIMIT,
    WSD_DETAIL_SERVICE_ENVELOPE_LIMIT,
    WSD_DETAIL_TARGET_ALREADY_EXISTS,
    WSD_DETAIL_TYPE_MISMATCH,
    WSD_DETAIL_UNEXPECTED_SELECTORS,
    WSD_DETAIL_UNREPORTABLE_SUCCESS,
    WSD_DETAIL_UNUSABLE_ADDRESS,
    WSD_DETAIL_URI_LIMIT_EXCEEDED,
    WSD_DETAIL_WHITESPACE,

    // WS-Addressing
    WSA_DETAIL_DUPLICATE_MESSAGE_ID,

    // SOAP
    SOAP_DETAIL_HEADER_NOT_UNDERSTOOD,

    // WS-Trust
    WST_DETAIL_UNSUPPORTED_TOKENTYPE,

    // WS_Policy
    WSP_DETAIL_INVALID_EPR,

    // OpenWsd
    WSD_DETAIL_ENDPOINT_ERROR,
    WSD_NO_DETAILS,
    WSD_SYSTEM_ERROR /* keep this as last entry ! */		
};
typedef enum __WsdFaultDetailType WsdFaultDetailType;


typedef enum {
    WSD_STATUS_NONE,

    /* Transport Errors */
    WSD_STATUS_CANCELLED                       = 1,
    WSD_STATUS_CANT_RESOLVE,
    WSD_STATUS_CANT_RESOLVE_PROXY,
    WSD_STATUS_CANT_CONNECT,
    WSD_STATUS_CANT_CONNECT_PROXY,
    WSD_STATUS_SSL_FAILED,
    WSD_STATUS_IO_ERROR,
    WSD_STATUS_MALFORMED,
    WSD_STATUS_TRY_AGAIN,

    /* HTTP Status Codes */
    WSD_STATUS_CONTINUE                        = 100,
    WSD_STATUS_SWITCHING_PROTOCOLS             = 101,
    WSD_STATUS_PROCESSING                      = 102, /* WebDAV */

    WSD_STATUS_OK                              = 200,
    WSD_STATUS_CREATED                         = 201,
    WSD_STATUS_ACCEPTED                        = 202,
    WSD_STATUS_NON_AUTHORITATIVE               = 203,
    WSD_STATUS_NO_CONTENT                      = 204,
    WSD_STATUS_RESET_CONTENT                   = 205,
    WSD_STATUS_PARTIAL_CONTENT                 = 206,
    WSD_STATUS_MULTI_STATUS                    = 207, /* WebDAV */

    WSD_STATUS_MULTIPLE_CHOICES                = 300,
    WSD_STATUS_MOVED_PERMANENTLY               = 301,
    WSD_STATUS_FOUND                           = 302,
    WSD_STATUS_MOVED_TEMPORARILY               = 302, /* RFC 2068 */
    WSD_STATUS_SEE_OTHER                       = 303,
    WSD_STATUS_NOT_MODIFIED                    = 304,
    WSD_STATUS_USE_PROXY                       = 305,
    WSD_STATUS_NOT_APPEARING_IN_THIS_PROTOCOL  = 306, /* (reserved) */
    WSD_STATUS_TEMPORARY_REDIRECT              = 307,

    WSD_STATUS_BAD_REQUEST                     = 400,
    WSD_STATUS_UNAUTHORIZED                    = 401,
    WSD_STATUS_PAYMENT_REQUIRED                = 402, /* (reserved) */
    WSD_STATUS_FORBIDDEN                       = 403,
    WSD_STATUS_NOT_FOUND                       = 404,
    WSD_STATUS_METHOD_NOT_ALLOWED              = 405,
    WSD_STATUS_NOT_ACCEPTABLE                  = 406,
    WSD_STATUS_PROXY_AUTHENTICATION_REQUIRED   = 407,
    WSD_STATUS_PROXY_UNAUTHORIZED              = WSD_STATUS_PROXY_AUTHENTICATION_REQUIRED,
    WSD_STATUS_REQUEST_TIMEOUT                 = 408,
    WSD_STATUS_CONFLICT                        = 409,
    WSD_STATUS_GONE                            = 410,
    WSD_STATUS_LENGTH_REQUIRED                 = 411,
    WSD_STATUS_PRECONDITION_FAILED             = 412,
    WSD_STATUS_REQUEST_ENTITY_TOO_LARGE        = 413,
    WSD_STATUS_REQUEST_URI_TOO_LONG            = 414,
    WSD_STATUS_UNSUPPORTED_MEDIA_TYPE          = 415,
    WSD_STATUS_REQUESTED_RANGE_NOT_SATISFIABLE = 416,
    WSD_STATUS_INVALID_RANGE                   = WSD_STATUS_REQUESTED_RANGE_NOT_SATISFIABLE,
    WSD_STATUS_EXPECTATION_FAILED              = 417,
    WSD_STATUS_UNPROCESSABLE_ENTITY            = 422, /* WebDAV */
    WSD_STATUS_LOCKED                          = 423, /* WebDAV */
    WSD_STATUS_FAILED_DEPENDENCY               = 424, /* WebDAV */

    WSD_STATUS_INTERNAL_SERVER_ERROR           = 500,
    WSD_STATUS_NOT_IMPLEMENTED                 = 501,
    WSD_STATUS_BAD_GATEWAY                     = 502,
    WSD_STATUS_SERVICE_UNAVAILABLE             = 503,
    WSD_STATUS_GATEWAY_TIMEOUT                 = 504,
    WSD_STATUS_HTTP_VERSION_NOT_SUPPORTED      = 505,
    WSD_STATUS_INSUFFICIENT_STORAGE            = 507, /* WebDAV search */
    WSD_STATUS_NOT_EXTENDED                    = 510  /* RFC 2774 */
} WsdKnownStatusCode;



struct __WsdFaultCodeTable
{
  WsdFaultCodeType  fault_code;
  WsdKnownStatusCode http_code;
  char*               fault_action;
  char*               subCodeNs;
  char*               code;
  char*               subCode;
  char*               reason;
};
typedef struct __WsdFaultCodeTable WsdFaultCodeTable;

struct __WsdFaultDetailTable
{
  WsdFaultDetailType  fault_code;
  char*   detail;
};
typedef struct __WsdFaultDetailTable WsdFaultDetailTable;


#endif /*WSD_FAULTS_H_*/
