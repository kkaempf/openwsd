/*******************************************************************************
 * Copyright (C) 2004-2006 Intel Corp. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  - Neither the name of Intel Corp. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL Intel Corp. OR THE CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

/**
 * @author Anas Nashif
 */

#ifndef WSDCLIENT_H_
#define WSDCLIENT_H_


#ifdef __cplusplus
extern "C" {
#endif				/* __cplusplus */

#ifndef WIN32
#include <pthread.h>
#endif				/* // !WIN32 */

#include "u/buf.h"
#include "wsd-client-api.h"
#include "wsd-xml-serialize.h"
#define WSD_CLIENT_BUSY       0x0001

	typedef struct {
		char *hostname;
		unsigned int port;
		char *path;
		char *user;
		char *pwd;
		char *scheme;
		char *endpoint;
		unsigned int auth_method;
		long auth_set;
		int status;
	} WsdClientData;


	struct _WsdAuthData {
		char *cainfo;
		char *caoid;
		unsigned char certificatethumbprint[20];
#ifdef _WIN32
		BOOL calocal;
#endif
		char *capath;
		char *sslcert;
		char *sslkey;
		unsigned int verify_peer;
		unsigned int verify_host;
	        wsdc_auth_request_func_t auth_request_func;
	        char *method;

		unsigned int crl_check;
		char *crl_file;
	};
	typedef struct _WsdAuthData WsdAuthData;

	struct _WsdProxyData {
		char *proxy;
		char *proxy_auth;
		char *proxy_username;
		char *proxy_password;
	};
	typedef struct _WsdProxyData WsdProxyData;

  /*
   * Client descriptor
   */

  struct _WsdClient {
    WsdClientData data; /* where's the client */
    WsdProxyData proxy_data;
    WsdAuthData authentication;
    char *from_urn;
    void *hdl;
    int flags;
    WsSerializerContextH serctx;
#ifdef _WIN32
    void* session_handle;
    long lock_session_handle;
#endif
    void *transport;
    char *content_encoding;
    unsigned long transport_timeout;
    char *user_agent;
    FILE *dumpfile;
    long initialized;
#ifndef _WIN32
    char *client_config_file;
#endif
  };

  struct _WsdRequestOptions {
    unsigned long flags;
    char *fragment;
    char *delivery_uri;
    char *reference;
    char *delivery_username; // username for delivery, if it is necessary
    char *delivery_password; // password for delivery, if it is necessary
    char *delivery_certificatethumbprint; // certificate thumbprint of event sink, if it is necessary
    float heartbeat_interval;
    float expires;
    unsigned int timeout;
    unsigned int max_envelope_size;
    unsigned int max_elements;
    hash_t *options; /* for WSM_OPTION_SET */
    char *locale; /* Sect. 6.3: Wsd:Locale */
  };

  /*
   * in-flight request
   */
  struct _WsdRequest {
    struct _WsdClient *client;
    struct _WsdRequestOptions *options;
    WsdAction action;
    WsXmlDocH request_doc;
    WsXmlNodeH node; /* response-specific node */
    u_buf_t *request_buf;
    WsXmlDocH response_doc;
    u_buf_t *response_buf;
    long response_code;
    char *fault_string;
    WS_LASTERR_Code last_error;
  };

#ifdef __cplusplus
}
#endif				/* __cplusplus */
#endif				/* WSDCLIENT_H_ */
