/*******************************************************************************
 * Copyright (C) 2004-2006 Intel Corp. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  - Neither the name of Intel Corp. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL Intel Corp. OR THE CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

/**
 * @author Anas Nashif
 */

#ifndef WSDCLIENT_API_H_
#define WSDCLIENT_API_H_


#ifdef __cplusplus
extern "C" {
#endif				/* __cplusplus */

#include "wsd-xml-api.h"
#include "wsd-names.h"
#include "wsd-types.h"
#include "wsd-xml-serializer.h"
#include "u/hash.h"
#include "u/list.h"
#include "u/buf.h"

/**
 * @defgroup Client Client
 * @brief WSD Client
 *
 * @{
 */


// Possible authentication methods

typedef enum {
    WS_NO_AUTH,
    WS_BASIC_AUTH,
    WS_DIGEST_AUTH,
    WS_PASS_AUTH,
    WS_NTLM_AUTH,
    WS_GSSNEGOTIATE_AUTH,
    WS_MAX_AUTH,
} wsdc_auth_type_t;

struct _WsdClient;
typedef struct _WsdClient WsdClient;

struct _WsdRequest;
typedef struct _WsdRequest WsdRequest;

struct _WsdRequestOptions;
typedef struct _WsdRequestOptions request_opt_t;

typedef void (*wsdc_auth_request_func_t)( WsdClient *client,
                                          wsdc_auth_type_t t,
                                          char **usr,
                                          char **pwd);

typedef enum {
  WS_LASTERR_OK = 0,
  WS_LASTERR_OTHER_ERROR,	// not recognized error
  WS_LASTERR_FAILED_INIT,
  WS_LASTERR_NO_REQUEST,
  WS_LASTERR_NO_CLIENT,
  WS_LASTERR_UNSUPPORTED_PROTOCOL,
  WS_LASTERR_URL_MALFORMAT,
  WS_LASTERR_COULDNT_RESOLVE_PROXY,
  WS_LASTERR_COULDNT_RESOLVE_HOST,
  WS_LASTERR_COULDNT_CONNECT,
  WS_LASTERR_HTTP_RETURNED_ERROR,
  WS_LASTERR_WRITE_ERROR,
  WS_LASTERR_READ_ERROR,
  WS_LASTERR_OUT_OF_MEMORY,
  WS_LASTERR_OPERATION_TIMEOUTED,	// the timeout time was reached
  WS_LASTERR_HTTP_POST_ERROR,
  WS_LASTERR_BAD_DOWNLOAD_RESUME,	//couldn't resume download
  WS_LASTERR_TOO_MANY_REDIRECTS,	// catch endless re-direct loops
  WS_LASTERR_SSL_CONNECT_ERROR,
  WS_LASTERR_SSL_PEER_CERTIFICATE,	// peer's certificate wasn't ok
  WS_LASTERR_SSL_ENGINE_NOTFOUND,	// SSL crypto engine not found
  WS_LASTERR_SSL_ENGINE_SETFAILED,	// can't set SSL crypto engine default
  WS_LASTERR_SSL_CERTPROBLEM,	// problem with the local certificate
  WS_LASTERR_SSL_CACERT,	// problem with the CA cert (path?)
  WS_LASTERR_SSL_ENGINE_INITFAILED,	// failed to initialise ENGINE
  WS_LASTERR_SEND_ERROR,	// failed sending network data
  WS_LASTERR_RECV_ERROR,	// failure in receiving network data
  WS_LASTERR_BAD_CONTENT_ENCODING,	// Unrecognized transfer encoding
  WS_LASTERR_LOGIN_DENIED,	// user, password or similar was not
  // accepted and we failed to login
  // 
  WS_LASTERR_BAD_CRL_FILE, //bad CRL file provided (Format, Path or permission)
  WS_LASTERR_CURL_BAD_FUNCTION_ARG, // bad function arg calling curl
  WS_LASTERR_LAST	// never use!
} WS_LASTERR_Code;
  
typedef enum {
  WSD_ACTION_NONE = 0,
  WSD_ACTION_CREATE_SCAN_JOB, 
  WSD_ACTION_RETRIEVE_IMAGE,
  WSD_ACTION_CANCEL_JOB,
  WSD_ACTION_VALIDATE_SCAN_TICKET,
  WSD_ACTION_GET_SCANNER_ELEMENTS,
  WSD_ACTION_GET_JOB_ELEMENTS,
  WSD_ACTION_GET_ACTIVE_JOBS,
  WSD_ACTION_GET_JOB_HISTORY
} WsdAction;

// options flags values
#define FLAG_NONE               0x0000
#define FLAG_DUMP_REQUEST       0x0001
#define FLAG_MUND_LOCALE        0x0002
#define FLAG_MUND_OPTIONSET     0x0004
#define FLAG_MUND_FRAGMENT      0x0008
#define FLAG_MUND_MAX_ESIZE     0x0010
#define FLAG_DUMP_RESPONSE      0x0020
#define FLAG_DEBUG_MODE         0x0040

struct _WsdFault  {
  const char *code;
  const char *subcode;
  const char *reason;
  const char *fault_detail;
};
typedef struct _WsdFault  WsdFault;

struct _WsdScanOptions {
    char *jobname;
    char *username;
    char *format;
    int images_to_transfer;
    char *input_source;
    char *content_type;
    char *front_color_mode;
    char *back_color_mode;
    int x_resolution;
    int y_resolution;
    int auto_size; /* if !=0, {x,y}_offset/width/height as not considered */
    int x_offset;
    int y_offset;
    int width;
    int height;
    int auto_exposure; /* if !=0, brightness/contrast/sharpness as not considered */
    int brightness;
    int contrast;
    int sharpness;
};
typedef struct _WsdScanOptions WsdScanOptions;

struct _WsdScanJob {
    char *id;
    char *token;
    char *document_name;
};
typedef struct _WsdScanJob WsdScanJob;

	/**
	 * Create a client using an endpoint as the argument
	 * @param endpoint an URL describing the endpoint, with user/pass, port and path
	 * @return client handle
	 */
	WsdClient *wsd_client_create_from_url(const char *endpoint);


	/**
 	* Create client and initialize context
 	* @param hostname Hostname or IP
 	* @param port port
 	* @param path HTTP path, for example /Wsd
 	* @param scheme scheme, HTTP or HTTPS
 	* @param username User name
 	* @param password Passwrod
 	* @return client handle
 	*/
	WsdClient *wsd_client_create(const char *hostname,
					 const int port, const char *path,
					 const char *scheme,
					 const char *username,
					 const char *password);

  /**
   * Create request to client
   * @param cl client
   * @param action WSD_ACTION_xxx
   * @param options request options
   * @param data action-specific data
   * @return request handle
   */
  WsdRequest * wsd_request_create(WsdClient *cl,
                                   WsdAction action,
                                   request_opt_t *options,
                                   void *data);
  void wsd_request_destroy(WsdRequest *rq);

  /**
   * Get scanner elements
   * @param client WsdClient from wsd_create()
   * @param options request_opt_t from wsd_create_options()
   * @param element which element to get, one of WSD_ELEMENT_xxx
   * @return WsdRequest
   */
  WsdRequest *wsd_action_get_scanner_element(WsdClient *cl, request_opt_t *options, char * element);

  WsdRequest *wsd_action_get_scanner_description(WsdClient *cl, request_opt_t *options);
  WsdRequest *wsd_action_get_scanner_configuration(WsdClient *cl, request_opt_t *options);
  WsdRequest *wsd_action_get_scanner_status(WsdClient * cl, request_opt_t *options);
  WsdRequest *wsd_action_create_scan_job(WsdClient * cl, request_opt_t *options, WsdScanOptions *scan_options);
  WsdRequest *wsd_action_retrieve_image(WsdClient * cl, request_opt_t *options, WsdScanJob *scan_job, u_buf_t **image_buf);
  WsdRequest *wsd_action_cancel_scan_job(WsdClient * cl, request_opt_t *options, WsdScanJob *scan_job);

	/**
	* Set request/response content encoding type. Default encoding type is "UTF-8"
	* @param cl Client handle
	* @param encoding type of encoding, for example "UTF-16"
	* @ return zero for success, others for an error
	*/
	int wsdc_set_encoding(WsdClient *cl, const char *encoding);

	/**
	 * Destroy client
	 * @param cl Client handle that was created with wsd_create_client
	 * @return void
	 */
	void wsd_client_destroy(WsdClient * cl);


	/* WsdClient handling */

	/**
	 * Get serialization context
	 * @param cl Client handle
	 * @return Context
	 */
	WsSerializerContextH wsdc_get_serialization_context(WsdClient * cl);

	/**
	 * Get host name from handle
	 * @param cl Client handle
	 * @return host name
	 */
	char *wsdc_get_hostname(WsdClient * cl);

	/**
	 * Get port from handle
	 * @param cl Client handle
	 * @return host name
	 */
	unsigned int wsdc_get_port(WsdClient * cl);

	/**
	 * Get uri path from handle
	 * @param cl Client handle
	 * @return uri path
	 */
	char *wsdc_get_path(WsdClient * cl);

	/**
	 * Get uri scheme from handle
	 * @param cl Client handle
	 * @return scheme
	 */
	char *wsdc_get_scheme(WsdClient * cl);

	/**
	 * Get username from handle
	 * @param cl Client handle
	 * @return username
	 */
	char *wsdc_get_user(WsdClient * cl);

	/**
	 * Get password from handle
	 * @param cl Client handle
	 * @return username
	 */
	char *wsdc_get_password(WsdClient * cl);

	/**
	* Get request/response content encoding type. Default encoding type is "UTF-8"
	* @param cl Client handle
	* @ return request encoding string
	*/
	char *wsdc_get_encoding(WsdClient *cl);

	/**
	 * Get endpoint from handle
	 * @param cl Client handle
	 * @return endpoint
	 */
	char *wsdc_get_endpoint(WsdClient * cl);

	/**
	 * Get response code from client
	 * @param cl Client handle
	 * @return response code
	 */
	long wsdc_get_response_code(WsdRequest *rq);

	/**
	 * Get fault string
	 * @param cl Client handle
	 * @return host name
	 */
	char *wsd_get_fault_string(WsdRequest *rq);

  int wsdc_in_debug_mode(WsdClient *cl);


  /**
   * get reponse document of request
   * @param rq request handle
   * @return WsXmlDocH
   */
  WsXmlDocH wsd_response_doc(WsdRequest *rq);

  /**
   * Get response node matching last call
   * @param rq Request handle
   * @return response node
   */
  WsXmlNodeH wsd_response_node(WsdRequest *rq);

  /**
   * Get response data
   * @param rq Request handle
   * @return response node
   */
  char *wsd_response_data(WsdRequest *rq);
  int wsd_response_size(WsdRequest *rq);

  /**
   * Get last error code
   * @param rq Request handle
   * @return last error code
   */
  WS_LASTERR_Code wsd_get_last_error(WsdRequest *rq);

  /*
   * Get string for WS_LASTERR_Code
   * @param error WS_LASTERR_Code
   * @return string representation for error code
   */
  char *wsd_transport_get_last_error_string(WS_LASTERR_Code err);

	/**
	 * Read XML file
	 * @param cl Client handle
	 * @param filename File name
	 * @param encoding Encoding
	 * @param XML options
	 * @return XML document
	 */
	WsXmlDocH wsdc_read_file( const char *filename,
					 const char *encoding,
					 unsigned long options);
	/**
	 * Read buffer into an XML document
	 * @param cl Client handle
	 * @param buf Buffer with xml text
	 * @param size Size of buffer
	 * @param encoding Encoding
	 * @param XML options
	 * @return XML document
	 */
	WsXmlDocH wsdc_read_memory( char *buf,
					   size_t size, const char *encoding,
					   unsigned long options);

	/**
	 * Create runtime context for client
	 * @param cl Client handle
	 * @return void
	 */
	WsContextH wsdc_create_runtime(void);

	/**
	 * Parse response and create a new envelope based on it
	 * @param cl Client handle
	 * @return New envelope
	 */
	WsXmlDocH wsd_build_envelope_from_response(WsdRequest *rq, u_buf_t **image_buf);

	/**
	 * Get enumeration context from response
	 * @param doc Response document
	 * @return enumeration context
	 */
	char *wsdc_get_enum_context(WsXmlDocH doc);

	/**
	* Get enumeration context from subscription response
	* @param doc Response document
	* @return enumeration context
	*/
	char *wsdc_get_event_enum_context(WsXmlDocH doc);

	/**
	 * Free enumeration context
	 * @param enumctx Enumeration context
	 * @return enumeration context
	 */
	void wsdc_free_enum_context(char * enumctx);

	/**
	 * Initialize client requeust options
	 * @return initialized options structure
	 */
	request_opt_t *wsd_options_create(void);

	/**
	 * destroy client request options
	 * @param options structure
	 * @return void
	 */
	void wsd_options_destroy(request_opt_t * op);

	void wsd_set_options_flags(request_opt_t * options,
				     unsigned long);

	unsigned long wsd_get_options_flags(request_opt_t * options);

	void wsd_clear_options_flags(request_opt_t * options,
				     unsigned long);

	void wsdc_set_options_from_uri(const char *resource_uri,
					request_opt_t * options);

        void wsdc_add_option(request_opt_t * options,
			     const char *key, const char *value);

        void wsdc_set_fragment(const char *fragment, request_opt_t * options);

        void wsdc_set_reference(const char *reference, request_opt_t * options);

  /* getter/setter for request locale, Section 6.3 of DSP0226 */
  void wsdc_set_locale(request_opt_t * options, const char *locale);
  char *wsdc_get_locale(request_opt_t * options);

	/* Misc */

	/* Place holder */
	void wsdc_remove_query_string(const char *resource_uri,
				       char **result);
	int wsdc_check_for_fault(WsXmlDocH doc);

	void wsdc_node_to_buf(WsXmlNodeH node, char **buf);

	char *wsdc_node_to_formatbuf(WsXmlNodeH node);

	void wsdc_get_fault_data(WsXmlDocH doc,
					 WsdFault  * fault);

	WsdFault  *wsdc_fault_new(void);


	void wsdc_fault_destroy(WsdFault  *fault);

	void wsdc_set_dumpfile(WsdClient *cl, FILE * f);

	FILE *wsdc_get_dumpfile(WsdClient *cl);

#ifndef _WIN32
	void wsdc_set_conffile(WsdClient *cl, char * f);

	char * wsdc_get_conffile(WsdClient *cl);
#endif
	void
	wsdc_set_delivery_uri(const char *delivery_uri, request_opt_t * options);


	void
	wsdc_set_sub_expiry(float event_subscription_expire, request_opt_t * options);


	void
	wsdc_set_heartbeat_interval(float heartbeat_interval, request_opt_t * options);

/** @} */


#ifdef __cplusplus
}
#endif				/* __cplusplus */
#endif				/* WSDCLIENT_H_ */
