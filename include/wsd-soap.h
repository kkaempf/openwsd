/*******************************************************************************
 * Copyright (C) 2004-2006 Intel Corp. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  - Neither the name of Intel Corp. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL Intel Corp. OR THE CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

/**
 * @author Anas Nashif
 * @author Eugene Yarmosh
 * @author Sumeet Kukreja, Dell Inc.
 * @author Liang Hou
 */

#ifndef WSD_SOAP_H_
#define WSD_SOAP_H_

#include "u/hash.h"
#include "u/list.h"
#include "wsd-faults.h"
#include "wsd-soap-message.h"
#include "wsd-xml-api.h"
#include "wsd-xml-serializer.h"

#define SOAP_MAX_RESENT_COUNT       10
#define PEDNING_EVENT_MAX_COUNT	10


#define WS_DISP_TYPE_MASK               0xffff

#define WS_DISP_TYPE_RAW_DOC            0
#define WS_DISP_TYPE_GET                1
#define WS_DISP_TYPE_PUT                2
#define WS_DISP_TYPE_CREATE         3
#define WS_DISP_TYPE_DELETE         4

#define WS_DISP_TYPE_ENUMERATE      5
#define WS_DISP_TYPE_PULL               6
#define WS_DISP_TYPE_RELEASE            7
#define WS_DISP_TYPE_UPDATE         8
#define WS_DISP_TYPE_GETSTATUS      9
#define WS_DISP_TYPE_COUNT              11
#define WS_DISP_TYPE_DIRECT_PULL         12
#define WS_DISP_TYPE_DIRECT_GET            13
#define WS_DISP_TYPE_GET_NAMESPACE  14
#define WS_DISP_TYPE_CUSTOM_METHOD  15
#define WS_DISP_TYPE_DIRECT_PUT            16
#define WS_DISP_TYPE_IDENTIFY           17
#define WS_DISP_TYPE_DIRECT_CREATE           18
#define WS_DISP_TYPE_DIRECT_DELETE           19
#define WS_DISP_TYPE_ENUM_REFINSTS           21
#define WS_DISP_TYPE_SUBSCRIBE		22
#define WS_DISP_TYPE_UNSUBSCRIBE		23
#define WS_DISP_TYPE_RENEW			24
#define WS_DISP_TYPE_EVT_PULL			25
#define WS_DISP_TYPE_ACK				26
#define WS_DISP_TYPE_PRIVATE                0xfffe



struct __SoapOp {
	unsigned        __undefined;
};
typedef struct __SoapOp *SoapOpH;

struct __Soap {
	/* do not move this field */
	pthread_mutex_t lockData;
	unsigned long   uniqueIdCounter;

	pthread_mutex_t lockSubs; //lock for Subscription Repository
	char 			*uri_subsRepository; //URI of repository
	WsContextH      cntx;
	void *listener;
};
typedef struct __Soap *SoapH;

struct _WsXmlDoc {
	void           *parserDoc;
	unsigned long   prefixIndex; // to enumerate not well known namespaces
};


struct _WS_CONTEXT_ENTRY {
	lnode_t        *node;
	unsigned long   size;
	unsigned long   options;
	char           *name;
};
typedef struct _WS_CONTEXT_ENTRY WS_CONTEXT_ENTRY;

struct _WS_CONTEXT {
	SoapH soap;
	unsigned long enumIdleTimeout;
	WsXmlDocH	indoc;
	hash_t *enuminfos;
	hash_t *entries;
	WsSerializerContextH serializercntx;
	list_t         	*subscriptionMemList; //memory Repository of Subscriptions
	/* to prevent user from destroying cntx he hasn't created */
	int             owner;
};

typedef struct __WsSubscribeInfo WsSubscribeInfo;
// EventThreadContext
struct __WsEventThreadContext {
	SoapH soap;
	WsSubscribeInfo *subsInfo;
	WsXmlDocH outdoc;
};
typedef struct __WsEventThreadContext * WsEventThreadContextH;

typedef void    (*WsProcType) (void);
struct __XmlSerializerInfo;


/* return values from wse_send_notification() */
#define WSE_NOTIFICATION_DROPEVENTS 1
#define WSE_NOTIFICATION_DRAOPEVENTS WSE_NOTIFICATION_DROPEVENTS /* sic! */
#define WSE_NOTIFICATION_HEARTBEAT 2
#define WSE_NOTIFICATION_NOACK 3
#define WSE_NOTIFICATION_EVENTS_PENDING 4

#if 0
enum __WsdFilterDialect {
	WSD_FILTER_XPATH,
	WSD_FILTER_SELECTOR
};
typedef enum __WsdFilterDialect WsdFilterDialect;


enum __WsdPolymorphismMode {
	INCLUDE_SUBCLASS_PROP = 1,
	EXCLUDE_SUBCLASS_PROP,
	POLYMORPHISM_NONE
};
typedef enum __WsdPolymorphismMode WsdPolymorphismMode;
#endif


typedef int     (*SoapServiceCallback) (SoapOpH, void *, void *);
struct __callback_t {
	lnode_t         node;
	              //dataBuf is passed to callback as data
	                SoapServiceCallback proc;
};
typedef struct __callback_t callback_t;

callback_t     *
make_callback_entry(SoapServiceCallback proc,
		    void *data,
		    list_t * list_to_add);



SoapH ws_soap_initialize(void);

void ws_set_context_enumIdleTimeout(WsContextH cntx,
                            unsigned long timeout);

void soap_destroy(SoapH soap);

SoapH ws_context_get_runtime(WsContextH hCntx);


SoapOpH
soap_create_op(SoapH soap,
	       char *inboundAction,
	       char *outboundAction,
	       char *role,
	       SoapServiceCallback callbackProc,
	       void *callbackData,
	       unsigned long flags);
void            soap_destroy_op(SoapOpH op);
WsXmlDocH       soap_get_op_doc(SoapOpH op, int inbound);
WsXmlDocH       soap_detach_op_doc(SoapOpH op, int inbound);
int             soap_set_op_doc(SoapOpH op, WsXmlDocH doc, int inbound);
SoapH           soap_get_op_soap(SoapOpH op);



WsContextH      ws_create_context(SoapH soap);

void            ws_initialize_context(WsContextH hCntx, SoapH soap);

WsContextH      ws_create_runtime(list_t * interfaces);

WsContextH      ws_create_ep_context(SoapH soap, WsXmlDocH doc);

WsContextH      ws_get_soap_context(SoapH soap);

int             ws_destroy_context(WsContextH hCntx);

const void     *get_context_val(WsContextH hCntx, const char *name);

const void     *ws_get_context_val(WsContextH cntx, const char *name, int *size);

unsigned long   ws_get_context_ulong_val(WsContextH cntx, char *name);

int             ws_set_context_ulong_val(WsContextH cntx, char *name, unsigned long val);

int             ws_set_context_xml_doc_val(WsContextH cntx, char *name, WsXmlDocH val);

int             ws_remove_context_val(WsContextH hCntx, char *name);

hnode_t        *

create_context_entry(hash_t * h,
		     char *name,
		     void *val);

void            destroy_context_entry(WS_CONTEXT_ENTRY * entry);

int             wsd_fault_occured(WsdMessage * msg);

WsdKnownStatusCode wsd_find_httpcode_for_value(WsXmlDocH doc);

WsdKnownStatusCode wsd_find_httpcode_for_fault_code(WsdFaultCodeType faultCode);


WsXmlDocH
wsd_generate_fault(WsXmlDocH inDoc,
		     WsdFaultCodeType faultCode,
		     WsdFaultDetailType faultDetail,
		     char *fault_msg);
void
wsd_get_fault_status_from_doc(WsXmlDocH doc, WsdStatus* status) ;

void
wsd_generate_fault_buffer( WsXmlDocH inDoc,
			    const char *encoding,
			    WsdFaultCodeType faultCode,
			    WsdFaultDetailType faultDetail,
			    char *fault_msg,
			    char **buf,
			    int *len);


void wsd_status_init(WsdStatus * s);

int wsd_check_status(WsdStatus * s);

void  wsd_timeouts_manager(WsContextH cntx, void *opaqueData);

void wsd_heartbeat_generator(WsContextH cntx, void *opaqueData);

WsEventThreadContextH ws_create_event_context(SoapH soap, WsSubscribeInfo *subsInfo, WsXmlDocH doc);

void * wse_notification_sender(void * thrdcntx);

void *wse_heartbeat_sender(void * thrdcntx);

void wse_notification_manager(void * cntx);

int outbound_addressing_filter(SoapOpH opHandle, void *data,
			       void *opaqueData);

int outbound_control_header_filter(SoapOpH opHandle, void *data,
				   void *opaqueData);

int soap_add_filter(SoapH soap, SoapServiceCallback callbackProc,
		    void *callbackData, int inbound);

WsdMessage *wsd_get_msg_from_op(SoapOpH op) ;

unsigned long wsd_get_maxsize_from_op(SoapOpH op);

#endif				/* SOAP_API_H_ */
