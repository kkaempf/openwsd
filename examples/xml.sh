HOST=192.168.0.112
#http://$HOST:80/StableWSDiscoveryEndpoint/schemas-xmlsoap-org_ws_2005_04_discovery
HEADER="Content-Type: application/soap+xml;"
curl -X POST -d @$1 http://$HOST:80/ws/\
  --header "$HEADER"\
  --dump-header headers \
  --trace-ascii -
#HEADER="Content-Type: application/soap+xml"
#curl -X POST -d @discovery.xml http://$HOST:80/wsd/scanservice.cgi \
#  --header "$HEADER"\
#  --dump-header headers \
#  --output discovery.out
