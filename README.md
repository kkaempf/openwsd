# Web Services for Devices

This is a C language implementation of [Web Services for Devices](https://en.wikipedia.org/wiki/Web_Services_for_Devices)
(WSD) standard.

The initial version targets the WSD-Scan standard and is used by the
[wsd-scan](https://gitlab.com/kkaempf/backends/tree/wsd-scan) driver
for [SANE](http://sane-project.org).


**CAUTION**

This is a preliminary release. The API **WILL CHANGE**.


## Building

Compilation requires [CMake](https://cmake.org) to build and
[libxml2](http://www.xmlsoft.org) for the [SOAP](https://en.wikipedia.org/wiki/SOAP) protocol


## Author

Klaus Kämpf, based on [Openwsman](https://github.com/Openwsman/openwsman)
